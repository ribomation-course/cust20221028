#include <iostream>
#include <utility>
#include <vector>
#include <string>
using std::cout;
using std::string;
using namespace std::string_literals;

template<typename T>
struct Container {
    std::vector<T> storage{};
public:
    template<typename... Args>
    void emplace(Args... args) {
        storage.emplace_back( std::forward<Args>(args)... );
    }
    auto begin() { return storage.begin(); }
    auto end() { return storage.end(); }
};

struct Account {
    string accno{};
    int balance{};
    Account(string const& a, int&& b) : accno{a}, balance{b} {}
};
auto operator<<(std::ostream &os, Account const &a) -> std::ostream & {
    return os << "Account{" << a.accno << ", " << a.balance << " kr}";
}

int main() {
    auto c = Container<Account>{};
    c.emplace("111-2222"s, 100);
    c.emplace("111-3333"s, 300);
    c.emplace("111-5555"s, 500);
    for (auto const& a : c) cout << a << "\n";
}
