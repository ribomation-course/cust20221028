#include <iostream>
#include <string>
#include "queue.hxx"
#include "person.hxx"

using std::cout;
using std::string;
using namespace std::string_literals;
using namespace ribomation;

int main() {
    {
        cout << "-- int --\n";
        auto      q = Queue<int>{};
        for (auto k = 1; !q.full(); ++k) q.put(k);
        while (!q.empty()) cout << q.get() << " ";
        cout << "\n";
    }

    {
        cout << "-- std::string --\n";
        auto      q = Queue<string>{};
        for (auto k = 1; !q.full(); ++k) {
            q.put("str"s + std::to_string(k));
        }
        while (!q.empty()) cout << q.get() << " ";
        cout << "\n";
    }

    {
        cout << "-- rm::person --\n";
        auto       q       = Queue<Person>{};
        string     names[] = {
                "Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s
        };
        auto const N       = sizeof(names) / sizeof(names[0]);

        for (auto k = 0; !q.full(); ++k) {
            q.put(Person(names[k % N], (k + 1) * 10));
        }
        while (!q.empty()) cout << q.get() << "\n";
        cout << "\n";
    }
}
