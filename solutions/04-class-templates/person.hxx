#pragma once

#include <string>
#include <utility>
#include <ostream>

namespace ribomation {
    using std::string;

    class Person {
        string   name{};
        unsigned age{};

    public:
        Person() = default;
        Person(string name, unsigned int age)
                : name(std::move(name)), age(age) {}

        [[nodiscard]]  string const& getName() const { return name; }
        [[nodiscard]] unsigned getAge() const { return age; }
    };

    auto operator<<(std::ostream& os, Person const& person) -> std::ostream& {
        os << "Person{name:" << person.getName() << ", age:" << person.getAge() << "}";
        return os;
    }
    
}


