#pragma once

namespace ribomation {
    constexpr unsigned CAPACITY = 10;

    template<typename E>
    class Queue {
        E   storage[CAPACITY]{};
        int size = 0, putIdx = 0, getIdx = 0;
    public:
        void put(E);
        E get();
        [[nodiscard]] unsigned length() const;
        [[nodiscard]] bool empty() const;
        [[nodiscard]] bool full() const;
    };

    template<typename E>
    void Queue<E>::put(E x) {
        storage[putIdx++] = x;
        putIdx %= CAPACITY;
        ++size;
    }

    template<typename E>
    E Queue<E>::get() {
        auto x = storage[getIdx++];
        getIdx %= CAPACITY;
        --size;
        return x;
    }

    template<typename E>
    unsigned Queue<E>::length() const {
        return size;
    }

    template<typename E>
    bool Queue<E>::empty() const {
        return length() == 0;
    }

    template<typename E>
    bool Queue<E>::full() const {
        return length() == CAPACITY;
    }
    
}
