#pragma once


namespace ribomation {

    template<typename ElemType,
            auto CAPACITY,
            template<typename, typename = std::allocator<ElemType>> class Container>
    class Queue {
        Container<ElemType> storage{};
    public:
        Queue() = default;

        unsigned size() const { return storage.size(); }
        unsigned capacity() const { return CAPACITY; }
        bool empty() const { return storage.empty(); }
        bool full() const { return size() == capacity(); }
        void put(ElemType x) { storage.push_back(x); }
        ElemType get() {
            auto x = storage.front();
            storage.pop_front();
            return x;
        } 
    };

}
