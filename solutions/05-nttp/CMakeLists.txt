cmake_minimum_required(VERSION 3.18)
project(05_nttp)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(queue queue.hxx app.cxx)
target_compile_options(queue PRIVATE ${WARN})
