#include <iostream>
#include <list>
#include <deque>
#include <string>
#include "queue.hxx"

using std::cout;
using std::string;
using namespace std::string_literals;
using namespace ribomation;

int main() {
    cout << std::boolalpha;
    {
        auto q = Queue<int, 10, std::list>{};
        cout << "q.size: " << q.size() << "\n";
        cout << "q.capacity: " << q.capacity() << "\n";
        cout << "q.empty: " << q.empty() << "\n";
        cout << "q.full: " << q.full() << "\n";
        q.put(42);
        cout << "q.get: " << q.get() << "\n";
    }

    {
        cout << "-- Queue<unsigned, 25, std::list> --\n";
        auto      q = Queue<unsigned, 25, std::list>{};
        for (auto k = 1U; !q.full(); ++k) q.put(k * 10);
        while (!q.empty()) cout << q.get() << " ";
        cout << "\n";
    }

    {
        cout << "-- Queue<long double, 15, std::deque> --\n";
        auto      q = Queue<long double, 15, std::deque>{};
        for (auto k = 1U; !q.full(); ++k) q.put(k * 3.1415926);
        while (!q.empty()) cout << q.get() << " ";
        cout << "\n";
    }

    {
        cout << "-- Queue<string, 10, std::deque> --\n";
        auto      q = Queue<string, 10, std::deque>{};
        for (auto k = 1U; !q.full(); ++k) q.put("{"s + std::to_string(k*123) + "}"s);
        while (!q.empty()) cout << q.get() << " ";
        cout << "\n";
    }

}
