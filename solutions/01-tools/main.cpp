#include <iostream>

short       glob1 = 42;
int         glob2 = 42;
long        glob3 = 42;
float       glob4 = 42;
double      glob5 = 42;
long double glob6 = 42;

int num;
int* ptr;

void func(short s, long double ld) {
    std::cout << "func: " << s << ld << "\n";
}

int main() {
    std::cout << "Hello, World!" << std::endl;
    func(17, 3.1415926);
    return 0;
}
