cmake_minimum_required(VERSION 3.18)
project(01_tools)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

add_executable(01_tools main.cpp)
target_compile_options(01_tools PRIVATE ${WARN})


