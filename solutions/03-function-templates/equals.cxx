#include <iostream>
#include <string>
using std::cout;
using std::string;
using namespace std::string_literals;

template<typename T>
bool equals(T a, T b) { return a == b; }

template<typename T>
bool equals(T a, T b, T c) { return equals(a, b) && equals(b, c); }


int main() {
    cout << std::boolalpha;
    {
        int x = 42, y = 40 + 2, z = 2 * 21;
        cout << "eq(" << x << "," << y << "," << z << ") --> " << equals(x, y, z) << "\n";
    }
    {
        long x = 42, y = 40 + 2, z = 2 * 21;
        cout << "eq(" << x << "," << y << "," << z << ") --> " << equals(x, y, z) << "\n";
    }
    {
        long double x = 42, y = 40 + 2, z = 2 * 21;
        cout << "eq(" << x << "," << y << "," << z << ") --> " << equals(x, y, z) << "\n";
    }
    {
        char x = 'Q', y = 'A' + 0x10, z = 'q' - 32;
        cout << "eq(" << x << "," << y << "," << z << ") --> " << equals(x, y, z) << "\n";
    }
    {
        string x = "wi-fi"s, y = "wi"s + "-"s + "fi"s, z = "wi"s + char(45) + "fi"s;
        cout << "eq(" << x << "," << y << "," << z << ") --> " << equals(x, y, z) << "\n";
    }
}
