#include <iostream>
#include <string>
#include <vector>
#include <cctype>
using std::cout;
using std::string;
using std::vector;
using namespace std::string_literals;

template<typename RetType, typename Iterator, typename MapFn, typename ReduceFn>
RetType map_reduce(Iterator first, Iterator last, MapFn mapper, ReduceFn reducer) {
    RetType result = mapper(*first);
    for (++first; first != last; ++first) result = reducer(result, mapper(*first));
    return result;
}

int main() {
    {
        int arr[]    = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        auto const N = sizeof(arr) / sizeof(arr[0]);
        auto r       = map_reduce<int>(arr, arr + N,
                                       [](auto x) { return x * x * x; },
                                       [](auto sum, auto n) { return sum + n; });
        cout << "(int) --> " << r << "\n";
    }
    
    {
        auto vec = vector<string>{"a"s, "b"s, "c"s, "d"s};
        auto r   = map_reduce<string>(vec.begin(), vec.end(),
                                      [](auto x) {
                                          x[0] = ::toupper(++x[0]);
                                          return x;
                                      },
                                      [](auto sum, auto n) { return sum + n; });
        cout << "(string) --> " << r << "\n";
    }
}
