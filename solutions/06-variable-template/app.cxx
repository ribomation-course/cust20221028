#include <iostream>
#include <string>

template<typename T>
struct is_floating_point {
    static constexpr bool value = false;
};

template<> struct is_floating_point<float> {
    static constexpr bool value = true;
};
template<> struct is_floating_point<double> {
    static constexpr bool value = true;
};
template<> struct is_floating_point<long double> {
    static constexpr bool value = true;
};

template<typename T>
constexpr bool is_floating_point_v = is_floating_point<T>::value;

int main() {
    static_assert(is_floating_point<float>::value);
    static_assert(is_floating_point_v<double>);
    static_assert(is_floating_point_v<long double>);

//    static_assert(is_floating_point_v<int>);
//    static_assert(is_floating_point_v<std::string>);
//    static_assert(is_floating_point_v<float*>);

    std::cout << "All compile-time tests passed\n";
}
