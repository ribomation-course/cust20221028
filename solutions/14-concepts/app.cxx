#include <iostream>
#include <string>
#include <vector>

template<typename Iterator>
concept is_iterator = requires(Iterator it, Iterator rhs) {
    *it;
    ++it;
    it != rhs;
};

template<typename Container>
concept is_iterable = requires(Container container) {
    typename Container::iterator;
    { container.begin() } -> is_iterator;
    { container.end() }  -> is_iterator;
};

template<typename T>
struct Whatever {
    struct iterator {};
};
static_assert(!is_iterator<Whatever<int>::iterator>);
static_assert(!is_iterable<Whatever<int>>);


template<typename ElemType, auto CAPACITY>
class Container {
    ElemType storage[CAPACITY]{};
    int putIdx = 0;
    int getIdx = 0;
    int count = 0;
public:
    Container() = default;
    auto size()     const { return count; }
    auto capacity() const { return CAPACITY; }
    bool empty()    const { return size() == 0; }
    bool full()     const { return size() == capacity(); }

    void put(ElemType x) {
        if (full()) throw std::overflow_error{""};
        storage[putIdx++] = x;
        putIdx %= capacity();
        ++count;
    }

    ElemType get() {
        if (empty()) throw std::underflow_error{""};
        auto x = storage[getIdx++];
        getIdx %= capacity();
        --count;
        return x;
    }

    struct iterator {
        Container<ElemType, CAPACITY> const *container;
        int current = 0;

        iterator(Container<ElemType, CAPACITY> const *c) : container{c} {}
        iterator(int c) : current{c} {}

        bool operator!=(iterator const &rhs) const { return current != rhs.current; }
        auto operator*() const { return container->storage[current]; }
        void operator++() { ++current; }
    };

    auto begin() const { return iterator{this}; }
    auto end() const { return iterator{size()}; }
};

static_assert(is_iterator<Container<int, 10>::iterator>);
static_assert(is_iterable<Container<int, 10>>);


void use_case(std::string const& name, is_iterable auto container) {
    using std::cout;
    cout << "--- " << name << " -----\n";
    int delim = 0;
    for (auto const &obj: container) cout << (delim++ ? ", " : "") << obj;
    cout << "\n";
}

int main() {
    using namespace std::string_literals;
    use_case("vector<int>"s, std::vector{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    use_case("vector<string>"s, std::vector{"Anna"s, "Britta"s, "Carina"s, "Diana"s, "Frida"s});
}
