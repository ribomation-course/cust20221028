#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <array>
#include <vector>
#include <cmath>
#include <cstdlib>

using namespace std::string_literals;

template<typename ValueType>
auto stringify(ValueType) -> std::string {
    return "??: "s + __PRETTY_FUNCTION__;
}

template<>
auto stringify(int value) -> std::string {
    return std::to_string(value);
}

template<>
auto stringify(double value) -> std::string {
//    char buf[64];
//    sprintf(buf, "%.2f", value);
//    return buf;

//    auto buf = std::ostringstream{};
//    buf << std::setprecision(3) << value;
//    return buf.str();

    auto buf = std::ostringstream{};
    buf << std::fixed << std::setprecision(2) << value;
    return buf.str();
}

template<>
auto stringify(bool value) -> std::string {
    return value ? "True"s : "False"s;
}

template<>
auto stringify(char const* value) -> std::string {
    return std::string{value};
}

template<>
auto stringify(std::string value) -> std::string {
    return value;
}

template<typename T, auto N>
auto stringify(std::array<T, N> const& value) -> std::string {
    auto buf   = std::ostringstream{};
    bool delim = false;
    buf << "[";
    for (auto n: value) {
        if (delim) buf << ", "; else delim = true;
        buf << n;
    }
    buf << "]";
    return buf.str();
}

template<typename T>
auto stringify(std::vector<T> const& value) -> std::string {
    auto buf   = std::ostringstream{};
    bool delim = false;
    buf << "[";
    for (auto n: value) {
        if (delim) buf << ", "; else delim = true;
        buf << n;
    }
    buf << "]";
    return buf.str();
}


int main() {
    std::cout << "int        : " << stringify(42) << "\n";
    std::cout << "double     : " << stringify(4 * ::atan(1)) << "\n";
    std::cout << "double     : " << stringify(4000 * ::atan(1)) << "\n";
    std::cout << "bool       : " << stringify(42 == (21 << 1)) << "\n";
    std::cout << "C string   : \"" << stringify("hello") << "\"\n";
    std::cout << "C++ string : \"" << stringify("hello"s) << "\"\n";
    std::cout << "std::array : " << stringify(std::array<int, 5>{10, 20, 30}) << "\n";
    std::cout << "std::vector: " << stringify(std::vector<long>{10, 20, 30}) << "\n";
}
