#include <stdio.h>

#define mk_equals2_fn(TYPE)  TYPE equals2_ ## TYPE(TYPE a, TYPE b) { \
    return (a) > (b) ? (a) : (b); \
}

#define mk_equals3_fn(TYPE)  TYPE equals3_ ## TYPE(TYPE a, TYPE b, TYPE c) { \
    return equals2_ ## TYPE(a, equals2_ ## TYPE(b, c));    \
}

mk_equals2_fn(int)
mk_equals3_fn(int)
mk_equals2_fn(double)
mk_equals3_fn(double)

int main() {
    {
        int x = 10, y = 17, z = 12;
        printf("max(%d, %d) --> %d\n", x, z, equals2_int(x, z));
        printf("max(%d, %d, %d) --> %d\n", x, y, z, equals3_int(x, y, z));
    }
    {
        double x = 2.718281828459, y = 3.141592654, z = 0.5772156649015;
        printf("max(%lf, %lf) --> %lf\n", x, z, equals2_double(x, z));
        printf("max(%lf, %lf, %lf) --> %lf\n", x, y, z, equals3_double(x, y, z));
    }
    return 0;
}
