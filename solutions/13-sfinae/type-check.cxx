#include "type-check.hxx"
using ribomation::types::is_iterator;
using ribomation::types::is_iterable;


template<typename T>
struct Whatever {
    struct iterator{};
};
static_assert(!is_iterator<Whatever<int>::iterator>());
static_assert(!is_iterable<Whatever<int>>());

