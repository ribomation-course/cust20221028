#include <iostream>
#include <string>
#include "container.hxx"

using namespace std::string_literals;
using std::cout;
using std::string;
using ribomation::types::Container;

int main() {
    {
        cout << "--- <int,10> ----\n";
        auto c = Container<int, 10>{};
        for (auto k = 1; !c.full(); ++k) c.put(k);
        for (auto n: c) cout << n << " ";
        cout << "\n";
        while (!c.empty()) cout << c.get() << " ";
        cout << "\n";
    }
    {
        cout << "--- <string,10> ----\n";
        auto c = Container<string, 6>{};
        for (auto k = 1; !c.full(); ++k) c.put("string-value-"s + std::to_string(k*1234567));
        for (auto n: c) cout << n << " ";
        cout << "\n";
        while (!c.empty()) cout << c.get() << " ";
        cout << "\n";
    }
}
