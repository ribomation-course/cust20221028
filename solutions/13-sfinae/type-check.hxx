#pragma once
#include <type_traits>

namespace ribomation::types {
    using std::declval;
    using std::void_t;

    namespace impl {
        template<typename T>
        using has_operator_star_t = decltype( *declval<T>() );

        template<typename T>
        using has_operator_plusplus_t = decltype( ++declval<T>() );

        template<typename T>
        using has_operator_inequals_t = decltype( declval<T>() != declval<T>() );

        template<typename T>
        using has_method_begin_t = decltype( declval<T>().begin() );

        template<typename T>
        using has_method_end_t = decltype( declval<T>().end() );
    }

    template<typename, typename = void>
    struct is_iterator : std::false_type {};

    template<typename IteratorType>
    struct is_iterator<IteratorType, void_t<
            impl::has_operator_star_t<IteratorType>,
            impl::has_operator_plusplus_t<IteratorType>,
            impl::has_operator_inequals_t<IteratorType>
    >> : std::true_type {};

    template<typename, typename = void>
    struct is_iterable : std::false_type {};

    template<typename ContainerType>
    struct is_iterable<ContainerType, void_t<
            impl::has_method_begin_t<ContainerType>,
            impl::has_method_end_t<ContainerType>
    >> : std::true_type {};

}
