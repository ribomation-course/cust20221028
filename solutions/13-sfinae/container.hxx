#pragma once
#include <stdexcept>
#include "type-check.hxx"

namespace ribomation::types {
    using ribomation::types::is_iterator;
    using ribomation::types::is_iterable;

    template<typename ElemType, auto CAPACITY>
    class Container {
        ElemType storage[CAPACITY]{};
        int putIdx = 0;
        int getIdx = 0;
        int count  = 0;
    public:
        Container() = default;
        auto size()     const {return count;}
        auto capacity() const {return CAPACITY;}
        bool empty()    const {return size() == 0;}
        bool full()     const {return size() == capacity();}

        void put(ElemType x) {
            if (full()) throw std::overflow_error{""};
            storage[putIdx++] =x;
            putIdx %= capacity();
            ++count;
        }

        ElemType get() {
            if (empty()) throw std::underflow_error{""};
            auto x = storage[getIdx++];
            getIdx %= capacity();
            --count;
            return x;
        }

        struct iterator {
            Container<ElemType,CAPACITY> const* container;
            int current = 0;
            iterator(Container<ElemType,CAPACITY> const* c) : container{c} {}
            iterator(int c) : current{c} {}
            bool operator !=(iterator const& rhs) const {return current != rhs.current;}
            auto operator *() const {return container->storage[current];}
            void operator ++() {++current;}
        };
        auto begin() const {return iterator{this};}
        auto end()   const {return iterator{size()};}
    };

    static_assert(is_iterator<Container<int,10>::iterator>());
    static_assert(is_iterable<Container<int,10>>());

}
