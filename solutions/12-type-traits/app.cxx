#include <iostream>
#include <type_traits>
#include <string>
#include <cstring>

using std::cout;
using namespace std::string_literals;

template<typename ElemType>
class Container {
    ElemType *storage = nullptr;
    unsigned capacity_ = 2;
    unsigned size_ = 0;
    bool full() const { return size() == capacity(); }

    void realloc() {
        auto old_storage = storage;
        auto old_capacity = capacity_;
        capacity_ <<= 1;
        storage = new ElemType[capacity_];

        if constexpr (std::is_trivially_copyable_v<ElemType>) {
            cout << "** mem cpy\n";
            std::memcpy(storage, old_storage, old_capacity);
        } else if constexpr (std::is_move_assignable_v<ElemType>) {
            cout << "** for mov\n";
            for (auto k = 0U; k < size(); ++k) storage[k] = std::move(old_storage[k]);
        } else {
            cout << "** for cpy\n";
            for (auto k = 0U; k < size(); ++k) storage[k] = old_storage[k];
        }

        delete[] old_storage;
    }

public:
    Container() { storage = new ElemType[capacity()]; }
    auto size() const { return size_; }
    auto capacity() const { return capacity_; }
    bool empty() const { return size() == 0; }

    void push(ElemType x) {
        if (full()) realloc();
        storage[size_++] = x;
    }

    ElemType pop() { return storage[--size_]; }

    struct iterator {
        Container<ElemType> *container = nullptr;
        unsigned current = 0;
        iterator(Container<ElemType> *c) : container(c) {}
        iterator(unsigned n) : current(n) {}
        bool operator!=(iterator const &rhs) const { return current != rhs.current; }
        ElemType operator*() const { return container->storage[current]; }
        void operator++() { ++current; }
    };

    auto begin() const { return iterator{this}; }
    auto end() const { return iterator{size()}; }
};

void usecase_int() {
    cout << "--- int ------\n";
    auto c = Container<int>{};
    auto const N = 5;
    for (auto k = 1; k <= N; ++k) {
        c.push(k);
        cout << "push: [" << c.size() << ", " << c.capacity() << "] " << k << "\n";
    }
    while (!c.empty()) {
        cout << "pop:  [" << c.size() << ", " << c.capacity() << "] " << c.pop() << "\n";
    }
}

void usecase_string() {
    cout << "--- string ------\n";
    auto c = Container<std::string>{};
    auto const N = 5;
    for (auto k = 1; k <= N; ++k) {
        c.push("str-"s + std::to_string(k));
        cout << "push: [" << c.size() << ", " << c.capacity() << "] " << k << "\n";
    }
    while (!c.empty()) {
        cout << "pop:  [" << c.size() << ", " << c.capacity() << "] " << c.pop() << "\n";
    }
}

struct Account {
    std::string accno{};
    int balance{};
    Account() = default;
    Account(Account&&) = delete;
    auto operator =(Account&&) -> Account& = delete;

    Account(std::string a, int b) : accno{std::move(a)}, balance{b} {}

    Account(Account const& rhs) : accno{rhs.accno}, balance{rhs.balance} {}
    auto operator =(Account const& rhs) -> Account& {
        accno = rhs.accno;
        balance = rhs.balance;
        return *this;
    }
};

auto operator<<(std::ostream &os, Account const &a) -> std::ostream & {
    return os << "Account{" << a.accno << ", " << a.balance << "}";
}

void usecase_account() {
    cout << "--- account ------\n";
    auto c = Container<Account>{};
    auto const N = 5;
    for (auto k = 1; k <= N; ++k) {
        auto a = Account{"bnk-"s + std::to_string(k * 37), k * 125};
        c.push(a);
        cout << "push: [" << c.size() << ", " << c.capacity() << "] " << k << "\n";
    }
    while (!c.empty()) {
        cout << "pop:  [" << c.size() << ", " << c.capacity() << "] " << c.pop() << "\n";
    }
}

int main() {
//    usecase_int();
//    usecase_string();
    usecase_account();
}
