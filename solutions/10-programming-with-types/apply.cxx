#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <cctype>

using std::cout;
using namespace std::string_literals;

template<typename Container, typename UnaryFn>
auto apply(Container& container, UnaryFn fn) -> Container& {
    for (auto& x: container) x = fn(x);
    return container;
}

template<typename KeyType, typename ValueType, typename UnaryFn>
auto apply(std::map<KeyType, ValueType>& container, UnaryFn fn) -> std::map<KeyType, ValueType>& {
    for (auto& [key, val]: container) val = fn(val);
    return container;
}


#define PRINT(C)  int delim = 0; for (auto const& x: C) os << (delim++ ? ", " : "[") << x; return os << "]";

template<typename E>
auto operator<<(std::ostream& os, std::vector<E> const& container) -> std::ostream& { PRINT(container) }

template<typename E>
auto operator<<(std::ostream& os, std::list<E> const& container) -> std::ostream& { PRINT(container) }

template<typename K, typename V>
auto operator<<(std::ostream& os, std::pair<K, V> const& pair) -> std::ostream& { 
    return os << pair.first << ":" << pair.second;
}

template<typename K, typename V>
auto operator<<(std::ostream& os, std::map<K, V> const& container) -> std::ostream& { PRINT(container) }


int main() {
    {
        auto vec = std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        auto fn  = [](auto n) { return n * n; };
        cout << "vec: " << vec << " --> " << apply(vec, fn) << "\n";
    }
    {
        auto lst = std::list<std::string>{"anna"s, "berit"s, "carin"s};
        auto fn  = [](auto s) { return apply(s, [](char c) { return ::toupper(c); }); };
        cout << "lst: " << lst << " --> " << apply(lst, fn) << "\n";
    }
    {
        auto tbl = std::map<std::string, unsigned>{
                {"anna"s,  11U}, {"berit"s, 22U}, {"carin"s, 33U}, {"doris"s, 44U}
        };
        auto fn  = [](auto val) { return val / 11U; };
        cout << "tbl: " << tbl << " --> " << apply(tbl, fn) << "\n";
    }
}
