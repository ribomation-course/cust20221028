#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::string;
using std::vector;
using namespace std::string_literals;


template<typename Head, typename... Tail>
bool equals(Head head, Tail... tail) {
    //cout << __PRETTY_FUNCTION__ << "\n" ;
    if constexpr (sizeof...(tail) == 0) return true;
    return ((head == tail) && ...);
}

int main() {
    cout << std::boolalpha;
    {
        cout << "42, 21 << 2: " << equals(42, 21 << 1) << "\n";
        cout << "42, 42, 42: " << equals(42, 42, 42) << "\n";
        cout << "42, 40 + 2, 2 * 3 * 7, 21 << 1: " << equals(42, 40 + 2, 2 * 3 * 7, 21 << 1) << "\n";
        cout << "42: " << equals(42) << "\n";
        cout << "42, 41: " << equals(42, 41) << "\n";
        cout << "42, 43, 42: " << equals(42, 43, 42) << "\n";
        
        //(42 == 42) --> 1/0 == 42
    }
    {
        cout << R"("HiFi"s, "Hi"s + "Fi"s: )" << equals("HiFi"s, "Hi"s + "Fi"s) << "\n";
    }
    {
        auto v1 = vector<long>{1, 2, 3};
        auto v2 = vector<long>{1, 2, 3};
        cout << "v1 == v2: " << equals(v1, v2) << "\n";

        auto v3 = vector<long>{1, 2, 3};
        cout << "v1 == v2 == v3: " << equals(v1, v2, v3) << "\n";

        auto v4 = vector<long>{1, 2, 5};
        cout << "v1 == v2 == v4: " << equals(v1, v2, v4) << "\n";
    }
}
