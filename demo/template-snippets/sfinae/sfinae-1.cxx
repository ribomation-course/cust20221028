#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <memory>
#include <utility>

template<typename ElemType, size_t NUM_ELEMS>
auto getSize(ElemType(&array)[NUM_ELEMS]) -> std::size_t {
    std::cout << "getSize( T(&)[N] ) --> ";
    return NUM_ELEMS;
}


/*
template<typename Container>
auto getSize(Container const& container) -> typename Container::size_type {
    std::cout << "getSize(Container) --> ";
    return container.size();
}
*/


template<typename Container>
auto getSize(Container const& container) 
    -> decltype( container.size(), typename Container::size_type() ) 
{
    std::cout << "getSize(Container) ALT --> ";
    return container.size();
}

auto getSize(...) -> std::size_t {
    std::cout << "getSize(...) --> ";
    return 0;
}

int main() {
    {
        int arr[] = {1,2,3,4,5};
        std::cout << "arr.size: " << getSize(arr) << "\n";
    }
    {
        auto vec = std::vector<int>{1,2,3,4,5,6,7,8,9,10};
        std::cout << "vec.size: " << getSize(vec) << "\n";
    }
    {
        auto vec = std::vector<std::string>{"C++", "is", "cool"};
        std::cout << "vec.size: " << getSize(vec) << "\n";
    }
    {
        auto lst = std::list<float>{1.F, 1.5F, 2.F, 2.5F};
        std::cout << "lst.size: " << getSize(lst) << "\n";
    }
    {
        auto alloc = std::allocator<long double>{};
        std::cout << "alloc.size: " << getSize(std::ref(alloc)) << "\n";
    }
    {
        auto pair = std::pair<std::string, unsigned>{"Hamlet", 113};
        std::cout << "pair.size: " << getSize(std::ref(pair)) << "\n";
    }
    {
        auto number = 42U;
        std::cout << "number: " << getSize(number) << "\n";
    }
}
