#include <iostream>
#include <string>
#include <type_traits>
using std::cout;
using namespace std::string_literals;

namespace impl {
    template<typename Head, typename...>
    struct first_of { using type = Head; };
}
template<typename... Args>
using first_of = typename impl::first_of<Args...>::type;

template<typename Head, typename... Tail>
constexpr inline bool has_same_type =
    std::conjunction_v< std::is_same<Head, Tail>... >;

template<typename... Args>
auto sum(Args&&... args) noexcept
    -> std::enable_if_t< has_same_type<Args...>, first_of<Args...> > 
{
    return (... + args);
}

int main() {
    cout << "sum(1..10): " << sum(1,2,3,4,5,6,7,8,9,10) << "\n";
    cout << "doubles   : " << sum(1.1, 2.2, 3.3, 4.4, 5.5) << "\n";
    cout << "strings   : " << sum("Hi"s, "-"s, "Fi"s) << "\n";
    cout << "single int: " << sum(42) << "\n";
//    cout << "mixed 1   : " << sum(42, 42U) << "\n";
//    cout << "mixed 2   : " << sum(42, 42.0) << "\n";
}


