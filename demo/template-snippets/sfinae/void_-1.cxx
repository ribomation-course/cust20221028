#include <iostream>
#include <type_traits>
#include <string>
#include <vector>
#include <map>
#include <array>
#include <numeric>

template<typename T, typename = std::void_t<>>
struct is_stl_container : std::false_type {};

template<typename T>
struct is_stl_container<
        T,
        std::void_t<
            typename T::value_type,
            typename T::size_type,
            typename T::allocator_type,
            typename T::iterator,
            typename T::const_iterator,
            decltype( std::declval<T>().size() ),
            decltype( std::declval<T>().begin() ),
            decltype( std::declval<T>().end() ),
            decltype( std::declval<T>().cbegin() ),
            decltype( std::declval<T>().cend() )
            >
        > : std::true_type {};


static_assert(is_stl_container< std::vector<int> >());
static_assert(is_stl_container< std::map<std::string,unsigned> >());
static_assert(is_stl_container< std::string >());

struct not_a_container {};
static_assert(!is_stl_container< not_a_container >());
static_assert(!is_stl_container< std::array<int,17> >());

template<typename Container>
auto sum(Container const& container) 
    -> std::enable_if_t<is_stl_container<Container>{},
                        typename Container::value_type>
{
    return std::reduce(container.begin(), container.end());
}

int main() { 
    using std::cout;
    cout << "vec: " << sum( std::vector<int>{1,2,3,4,5} ) << "\n"; 
}

