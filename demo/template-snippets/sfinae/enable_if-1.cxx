#include <iostream>
#include <type_traits>
#include <initializer_list>
#include <iterator>
#include <vector>
using std::cout;

struct input_iterator_tag { };
struct forward_iterator_tag       : input_iterator_tag { };
struct bidirectional_iterator_tag : forward_iterator_tag { };
struct random_access_iterator_tag : bidirectional_iterator_tag { };


template<typename Iterator, typename = std::void_t<>>
struct iterator_traits_base { };

template<typename Iterator>
struct iterator_traits_base<
        Iterator,
        std::void_t<typename Iterator::iterator_category,
                    typename Iterator::value_type,
                    typename Iterator::difference_type,
                    typename Iterator::pointer,
                    typename Iterator::reference>>
{
    using iterator_category = typename Iterator::iterator_category;
    using value_type        = typename Iterator::value_type;
    using difference_type   = typename Iterator::difference_type;
    using pointer           = typename Iterator::pointer;
    using reference         = typename Iterator::reference;
};

template<typename Iterator>
struct iterator_traits : iterator_traits_base<Iterator> { };

template<typename Type>
struct iterator_traits<Type*> {
    using iterator_category = random_access_iterator_tag;
    using value_type        = Type;
    using difference_type   = ptrdiff_t;
    using pointer           = Type*;
    using reference         = Type&;
};



template<typename Iterator>
using get_iterator_category_t = 
        typename iterator_traits<Iterator>::iterator_category;

template<typename Iterator>
using require_input_iterator =
        std::enable_if_t<
            std::is_convertible_v<
                get_iterator_category_t<Iterator>,
                input_iterator_tag> >;



template<typename ElemType>
class SillyContainer {
    static constexpr unsigned N    = 100;
    ElemType                  elements[N]{};
    unsigned                  next = 0;
public:
    SillyContainer(unsigned numElems, ElemType value) {
        while (!full() && numElems-- > 0) push(value);
    }

    template<typename Iterator, typename = std::_RequireInputIter<Iterator>>
//    template<typename Iterator, typename = require_input_iterator<Iterator>>
    SillyContainer(Iterator first, Iterator last) {
        for (; !full() && first != last; ++first) push( *first );
    }

    [[nodiscard]] auto size() const { return next; }
    [[nodiscard]] bool full() const { return size() == N; }
    
    void push(ElemType x) {
        if (full()) throw std::overflow_error{"full"};
        elements[next++] = x;
    }

    ElemType operator[](unsigned idx) const {
        if (size() <= idx) throw std::out_of_range{"outside boundary"};
        return elements[idx];
    }
};

template<typename T>
auto operator<<(std::ostream& os, SillyContainer<T> const& c) -> std::ostream& {
    os << "[";
    for (auto k = 0U; k < c.size(); ++k) cout << (k > 0 ? ", " : "") << c[k];
    os << "]";
    return os;
}


int main() {
    {
        auto c = SillyContainer<int>{10, 42};
        cout << c << "\n";  //42,42,42,42,42,...
    }
    {
        int values[] = {2,3,5,7,11,13,17,19,23,29};
        constexpr auto N = sizeof(values) / sizeof(*values);
        auto c = SillyContainer<int>{values, values + N};
        cout << c << "\n";  //2,3,5,7,11,...
    }
    {
        auto vec = std::vector<int>{1,1,2,3,5,8,13,21,34,55};
        auto c = SillyContainer<int>(vec.begin(), vec.end());
        cout << c << "\n";
    }
}


