#include <iostream>


template<bool value>
struct TypePredicate {
    static constexpr bool valid = value;
    constexpr operator bool() const { return value; }
};
struct NotValid : TypePredicate<false> {};
struct Valid    : TypePredicate<true> {};

template<typename T1, typename T2> struct EQ : NotValid {};
template<typename T> struct EQ<T, T> : Valid {};
static_assert(EQ<int, int>{});
static_assert(!EQ<int, float>{});

template<typename T> struct isPointer     : NotValid {};
template<typename T> struct isPointer<T*> : Valid {};
static_assert(isPointer<char*>{});
static_assert(!isPointer<double>{});

template<typename T> struct isFloatingPoint : NotValid {};
template<> struct isFloatingPoint<float>       : Valid {};
template<> struct isFloatingPoint<double>      : Valid {};
template<> struct isFloatingPoint<long double> : Valid {};
static_assert(isFloatingPoint<float>{});
static_assert(!isFloatingPoint<int>{});


template<typename T> struct TypeTransform { 
    using type = T;
};

template<typename T> struct dropPointer : TypeTransform<T> {};
template<typename T> struct dropPointer<T*> : TypeTransform<T> {};
static_assert(EQ<int, dropPointer<int*>::type>{});

template<typename T> struct addPointer : TypeTransform<T> {
    using type = T*;
};
static_assert(EQ<int*, addPointer<int>::type>{});

template<typename T> struct dropConst : TypeTransform<T> {};
template<typename T> struct dropConst<T const> : TypeTransform<T> {};
static_assert(EQ<int, dropConst<int const>::type>{});
static_assert(EQ<int*, dropConst<int* const>::type>{});

template<typename T> struct addConst : TypeTransform<T> {
    using type = T const;
};
static_assert(EQ<int const, addConst<int>::type>{});



//std::remove_const_t<int>

int main() {}
