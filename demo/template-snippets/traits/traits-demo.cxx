#include <iostream>
#include <type_traits>

using namespace std;

auto fn(...) -> char const* {
    return "fn(...)";
}

template<typename T>
auto fn(T arg) 
    -> typename enable_if<is_integral_v<T>, char const*>::type 
{
    return "fn(integral)";
}

template<typename T>
auto fn(T arg) 
    -> typename enable_if<is_floating_point_v<T>, char const*>::type 
{
    return "fn(floating-point)";
}

auto fn(float f) -> char const* {
    return "fn(float)";
}


int main() {
    cout << fn(42)
         << ", " << fn(17U)
         << ", " << fn(1.5f)
         << ", " << fn(3.1415L)
         << ", " << fn(nullptr)
         << ", " << fn("hello")
         << "\n";
}


