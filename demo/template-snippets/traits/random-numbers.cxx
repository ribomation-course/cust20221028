#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <type_traits>
using namespace std;

template<typename ElemType,
        typename Distribution = conditional_t<
                is_integral_v<ElemType>,
                uniform_int_distribution<ElemType>,
                uniform_real_distribution<ElemType>>
>
auto populate(size_t numValues, ElemType minValue, ElemType maxValue) -> vector<ElemType> {
    static_assert(is_arithmetic_v<ElemType>, "Must use a numeric type");
    auto result = vector<ElemType>(numValues);
    auto distr  = Distribution{minValue, maxValue};
    auto r      = random_device{};
    generate_n(begin(result), numValues, [&r, &distr]() { return distr(r); });
    return result;
}

template<typename ElemType>
auto operator<<(ostream& os, vector<ElemType> const& vec) -> ostream& {
    os << "[";
    bool      delim = false;
    for (auto elem: vec) {
        if (delim) os << ", "; else delim = true;
        os << elem;
    }
    return os << "]";
};

int main() {
    auto const N = 10;
    cout << "ints   : " << populate(N, -100, +100) << "\n"; 
    cout << "floats : " << populate(N/2, 100.F, 1000.F) << "\n"; 
    cout << "letters: " << populate(N, 'A', 'Z') << "\n"; 
}


