#include <iostream>
#include <iomanip>
#include <vector>
#include <deque>
#include <list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <array>
#include <string>
#include <cstring>
using std::cout;

template<typename T, bool value>
struct TypePredicate {
    static constexpr bool valid = value;
    constexpr operator bool() const { return value; }
};
template<typename T> struct NotValid : TypePredicate<T, false> {};
template<typename T> struct Valid    : TypePredicate<T, true> {};


template<typename T> struct isPointer     : NotValid<T> {};
template<typename T> struct isPointer<T*> : Valid<T> {};

template<typename T> struct isBool       : NotValid<T> {};
template<>           struct isBool<bool> : Valid<bool> {};

template<typename T> struct isFloatingPoint : NotValid<T> {};
template<> struct isFloatingPoint<float>       : Valid<float> {};
template<> struct isFloatingPoint<double>      : Valid<double> {};
template<> struct isFloatingPoint<long double> : Valid<long double> {};

template<typename T> struct isString    : NotValid<T> {};
template<> struct isString<char*>       : Valid<char*> {};
template<> struct isString<char const*> : Valid<char const*> {};


template<typename Type>
void generic_print(Type arg) {
    if constexpr(isString<Type>{})
        cout << "\"" << arg << "\"";
    else if constexpr(isPointer<Type>{})
        cout << *arg << " @ " << arg;
    else if constexpr(isBool<Type>{})
        cout << (arg ? "true" : "false");
    else if constexpr(isFloatingPoint<Type>{})
        cout << std::setprecision(3) << arg;
    else 
        cout << arg;
    cout << "\n";
}

template<typename Type, size_t N>
void generic_print(const Type(& arr)[N]) {
    if constexpr(N == 0)
        cout << "[]\n";
    else if constexpr(N == 1)
        cout << "[" << *arr << "]\n";
    else {
        cout << "[" << arr[0];
        for (auto k = 1UL; k < N; ++k) cout << ", " << arr[k];
        cout << "]\n";
    }
}

template<typename Type, size_t N>
void generic_print(std::array<Type,N> const& arr) {
    if constexpr(N == 0)
        cout << "[]\n";
    else if constexpr(N == 1)
        cout << "[" << *arr << "]\n";
    else {
        cout << "[" << arr[0];
        for (auto k = 1UL; k < N; ++k) cout << ", " << arr[k];
        cout << "]\n";
    }
}

template<size_t N>
void generic_print(const char(& str)[N]) {
    cout << "\"" << str << "\" [" << N << "]\n";
}

template<typename Type>
struct Span {
    Type const* arr;
    size_t const N;
    Span(Type* arr_, size_t N_) : arr(arr_), N(N_) {}
};

template<typename Type>
void generic_print(Span<Type> wrapper) {
    if (wrapper.N == 0)
        cout << "[]\n";
    else if (wrapper.N == 1)
        cout << "[" << *wrapper.arr << "]\n";
    else {
        cout << "[" << wrapper.arr[0];
        for (auto k = 1UL; k < wrapper.N; ++k)
            cout << ", " << wrapper.arr[k];
        cout << "]\n";
    }
}

template<typename KeyType, typename ValType>
auto operator<<(std::ostream& os, std::pair<KeyType, ValType> const& elem) -> std::ostream& {
    return os << elem.first << ":" << elem.second;
}

template<template<typename, typename...> class Container, typename ElemType, typename... Args>
void generic_print(Container<ElemType, Args...> const& container) {
    cout << "[";
    bool delim = false;
    for (auto const& elem: container) {
        if (delim) cout << ", "; else delim = true;
        cout << elem;
    }
    cout << "]\n";
}

template<template<typename, typename...> class Container, typename... Args>
void generic_print(Container<char, Args...> const& str) {
    cout << "\"" << str << "\"\n";
}


int main() {
    auto i = 42;
    auto f = 3.141592654;
    auto b = true;
    auto str = "This is a C string";
    char str2[] = "This is an array C string";
    auto strdyn = strdup("This is a dynamic C string");
    auto strxx = std::string{"This is a C++ string"};
    int arr[] = {10,20,30,40};
    int* arrdyn = new int[3];
    arrdyn[0] = 100; arrdyn[1] = 200; arrdyn[2] = 300; 
    
    auto vec = std::vector<int>{1,2,3,4,5};
    auto dec = std::deque<std::string>{"C++", "is", "cool", "!"};
    auto lst = std::list<double>{1,2,3,4,5};
    
    auto set = std::set<std::string>{"C++", "is", "cool", "!"};
    auto uset = std::unordered_set<std::string>{"C++", "is", "cool", "!"};
    
    auto map = std::map<std::string, unsigned>{
            {"one",1}, {"two",4}, {"three",9}, {"four",16}, {"five",25}
    };
    auto umap = std::unordered_map<std::string, unsigned>{
            {"hi",2}, {"ho",3}, {"yo",5}, {"bye",7}, {"howdy",11}
    };
    
    auto stl_arr = std::array<int,5>{1,4,9,16,25};

    generic_print(i);
    generic_print(&i);
    generic_print(f);
    generic_print(b);
    generic_print(str);
    generic_print(str2);
    generic_print(strdyn);
    generic_print(strxx);
    generic_print(arr);
    generic_print(Span(arrdyn,3));
    
    generic_print(vec);
    generic_print(dec);
    generic_print(lst);
    
    generic_print(set);
    generic_print(uset);

    generic_print(map);
    generic_print(umap);

    generic_print(stl_arr);

}


