#include <iostream>
#include <boost/type_index.hpp>
namespace B = boost::typeindex;

int main() {
    int arr[] = {1,2,3,4,5};
    std::cout << "type(arr)=" << B::type_id_with_cvr<decltype(arr)>().pretty_name() << "\n";
    
    char s[123];
    std::cout << "type(s)="<< B::type_id_with_cvr<decltype(s)>().pretty_name() << "\n";
    
    auto dynarr = new double[37];
    std::cout << "type(dynarr)="<< B::type_id_with_cvr<decltype(dynarr)>().pretty_name() << "\n";
}


