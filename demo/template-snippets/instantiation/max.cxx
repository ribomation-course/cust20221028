#include <iostream>
#include <string>
#include <vector>
#include <cstring>
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::boolalpha;
using namespace std::string_literals;


template<typename T>
T MAX(T a, T b) {
    return a >= b ? a : b;
}

template<>
auto MAX(char const* a, char const* b) -> char const* {
    return (strcmp(a, b) > 0) ? a : b;
}

struct Square {
    template<typename T>
    auto operator()(T n) { return n*n; }
};

void useSquare() {
    auto fn = Square{};
    auto result = fn.operator()<int>(5);
    cout << result << endl;
}

//void useVector() {
//    auto vec = vector<int>{1,2,3};
//    cout << vec.size() << endl;
//}

extern template long MAX<long>(long, long);

template short MAX<short>(short, short);
template float MAX<float>(float, float);
template long MAX<long>(long, long);

int main() {
    cout << MAX<int>(17, 42) << endl;
    cout << MAX("Hi", "hi") << endl;
    useSquare();
}

