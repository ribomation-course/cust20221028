#include <iostream>

template<typename T>
struct Dummy {
    T value;
    Dummy() : value{0} {}
    Dummy(T v) : value{v} {}

    template<typename U>
    auto operator =(Dummy<U> const u) -> Dummy& {
        value = u.value;
        return *this;
    }
};

int main() {
    short from = 42;
    int to;
    to = from;
    std::cout << to << std::endl;
    
    Dummy<short> FROM{42};
    Dummy<int> TO;
    TO = FROM;
    std::cout << TO.value << std::endl;
}

