#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

class InstanceCount {
   inline static int count = 0;
protected:
    ~InstanceCount() { --count; }
    InstanceCount() { ++count; }
    InstanceCount(InstanceCount const&) { ++count; }
    InstanceCount(InstanceCount&&) noexcept { ++count; }
public:
    static int currentCount() { return count; }
};

struct Person : InstanceCount {
    string name;
    Person(string name_) : name{move(name_)} {}
};

struct Account : InstanceCount {
    string accno;
    int    balance;
    Account(string accno_, int initialBalance = 0)
            : accno{move(accno_)}, balance{initialBalance} {}
};

int main() {
    auto r           = random_device{};
    auto nextBalance = normal_distribution<float>{500, 250};
    auto nextChar    = uniform_int_distribution<char>{'a', 'z'};
    auto nextWord    = [&](unsigned numChars) {
        auto      word = string{};
        for (auto k = 0U; k < numChars; ++k) word += nextChar(r);
        return word;
    };

    auto const numPersons  = uniform_int_distribution<unsigned>{100, 1000}(r);
    auto const numAccounts = uniform_int_distribution<unsigned>{250, 2500}(r);
    cout << "@ persons : " << numPersons << endl;
    cout << "@ accounts: " << numAccounts << endl;

    cout << "^ persons : " << Person::currentCount() << endl;
    cout << "^ accounts: " << Account::currentCount() << endl;
    {
        auto persons = vector<Person>{};
        generate_n(back_inserter(persons), numPersons, [&]() {
            return Person{nextWord(8)};
        });
        cout << "# persons : " << Person::currentCount() << endl;

        auto accounts = vector<Account>{};
        generate_n(back_inserter(accounts), numAccounts, [&]() {
            return Account{nextWord(6), static_cast<int>(nextBalance(r))};
        });
        cout << "# accounts: " << Account::currentCount() << endl;
    }
    cout << "$ persons : " << Person::currentCount() << endl;
    cout << "$ accounts: " << Account::currentCount() << endl;
}


