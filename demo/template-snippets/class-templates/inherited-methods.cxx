#include <iostream>
#include <string>
using namespace std;

template<typename T>
class Vehicle {
    string licenseNo;
    T cargo{};
public:
    Vehicle(const string& licenseNo) : licenseNo(licenseNo) {}
    string getLicenseNo() const { return licenseNo; }
    T getCargo() const { return cargo; }
};

template<typename T>
class Car : public Vehicle<T> {
    bool turbo;
public:
    Car(const string& licenseNo, bool turbo = true) 
        : Vehicle<T>(licenseNo), turbo(turbo) {}
        
    using Vehicle<T>::getLicenseNo;
    void print() {
//        auto lic = this->getLicenseNo();
//        auto lic = Vehicle<T>::getLicenseNo();
        auto lic = getLicenseNo();
        cout << "Car<" << lic << ">\n";
    }    
};

int main() {
    auto volvo = Car<double>{"ABC123"s};
    volvo.print();    
}
