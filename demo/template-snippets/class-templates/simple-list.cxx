#include <iostream>
#include <string>

using namespace std;

template<typename PayloadType>
struct Node {
    PayloadType payload;
    Node* next;

    Node(PayloadType payload_, Node* next_) : payload{payload_}, next{next_} {}

    ~Node() { delete next; }

    friend auto operator<<(ostream& os, Node& first) -> ostream& {
        for (Node* iter = &first; iter != nullptr; iter = iter->next) {
            os << iter->payload << endl;
        }
        return os;
    }
};

struct Person {
    string   name;
    unsigned age;
    Person(string const& name_, unsigned age_) : name{name_}, age{age_} {}
    friend auto operator<<(ostream& os, Person const& p) -> ostream& {
        return os << "Person<" << p.name << ", " << p.age << ">";
    }
};

void personUsage() {
    Node<Person>* persons = nullptr;
    persons = new Node{Person{"Carin", 53}, persons};
    persons = new Node{Person{"Berit", 42}, persons};
    persons = new Node{Person{"Anna", 31}, persons};
    cout << *persons;
    delete persons;
}

struct Fibonacci {
    using ResType = unsigned long long;
    unsigned argument;
    ResType  result;

    Fibonacci(unsigned argument_) : argument{argument_} {
        ResType  f0 = 0, f1 = 1;
        unsigned n  = argument;
        while (--n > 0) {
            ResType f = f0 + f1; f0 = f1; f1 = f;
        }
        result = f1;
    }

    friend auto operator<<(ostream& os, Fibonacci const& f) -> ostream& {
        return os << "fib(" << f.argument << ") = " << f.result;
    }
};

void FibonacciUsage() {
    Node<Fibonacci>* fibValues = nullptr;
    for (auto n : {50U,40U,30U,20U,10U}) {
        fibValues = new Node{Fibonacci{n}, fibValues};
    }
    cout << *fibValues;
    delete fibValues;
}

int main() {
    personUsage();
    FibonacciUsage();
}

