#include <iostream>
#include <string>
using namespace std;

constexpr unsigned CAPACITY = 10;
template<typename ElemType>
class Stack {
    ElemType storage[CAPACITY]{};
    int      idx = 0;
public:
    void     push(ElemType x);
    ElemType pop() ;
    bool     empty() const;
    bool     full()  const;
    Stack&   operator =(Stack const& rhs);
};

template<typename T>
void Stack<T>::push(T x) { storage[idx++] = x; }

template<typename E>
E Stack<E>::pop() { return storage[--idx]; }

template<typename E>
bool Stack<E>::empty() const { return idx == 0; }

template<typename E>
bool Stack<E>::full() const  { return idx == CAPACITY; }

template<typename X>
Stack<X>& Stack<X>::operator =(Stack const& rhs) {
    for (auto k = 0U; k < CAPACITY; ++k) storage[k] = rhs.storage[k];
    idx = rhs.idx;
    return *this;
}



template<typename T, typename Function>
void populate(Stack<T>& stk, Function fn) {
    for (short k = 1; !stk.full(); ++k) stk.push(fn(k));
}

template<typename T>
void drain(Stack<T>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}

int main() {
    {
        auto     stk = Stack<short>{};
        populate(stk, [](auto k){ return k * 42; });
        drain(stk);
    }
    {
        auto     stk = Stack<long double>{};
        populate(stk, [](auto k){ return k * 3.141592654; });
        drain(stk);
    }
    {
        auto     stk = Stack<string>{};
        populate(stk, [](auto k){ return "str-"s + to_string(k); });
        auto stk2 = Stack<string>{};
        stk2 = stk;
        drain(stk);
        drain(stk2);
    }
}
