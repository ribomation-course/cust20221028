#include <iostream>
using namespace std;

struct BaseNonTemplate { int value{}; };

template<typename T>
struct BaseTemplate { T value{}; };

template<typename T>
struct SubTemplate1 : BaseNonTemplate {
    T subValue{};
};

template<typename T, typename U>
struct SubTemplate2 : BaseTemplate<U> {
    T subValue{};
};


int main() {
    auto obj1 = BaseNonTemplate{};
    auto obj2 = BaseTemplate<int>{};
    auto obj3 = SubTemplate1<long>{};
    auto obj4 = SubTemplate2<float, double>{};
    
    cout << obj1.value << endl;
    cout << obj2.value << endl;
    cout << obj3.subValue << ":" << obj3.value << endl;
    cout << obj4.subValue << ":" << obj4.value << endl;
}

