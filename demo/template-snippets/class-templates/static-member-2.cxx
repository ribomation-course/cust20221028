#include <iostream>
#include <string>
using namespace std;

template<typename T>
struct Dummy {
    inline static int intVar{37};
    inline static T   typeVar{};
};

int main() {
    {
        auto obj = Dummy<double>{};
        obj.typeVar = 3.1415926;
        cout << "obj1: " << obj.intVar << ", " << obj.typeVar << endl;
    }
    {
        auto obj    = Dummy<long>{};
        obj.intVar  = 42;
        obj.typeVar = 1234567890;
        cout << "obj2: " << obj.intVar << ", " << obj.typeVar << endl;
    }
    {
        auto obj    = Dummy<string>{};
        obj.intVar  = 17;
        obj.typeVar = "Hello"s;
        cout << "obj3: " << obj.intVar << ", " << obj.typeVar << endl;
    }
}



