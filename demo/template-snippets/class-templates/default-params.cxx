#include <iostream>
using namespace std;

template<typename A, typename B = int, typename C = long>
struct Dummy {
    A a{};
    B b{};
    C c{};
};

//template<typename A, typename B = double, typename C> 
//ERR: Template parameter missing a default argument
//struct Dummy2 {
//    A a{};
//    B b{};
//    C c{};
//};


int main() {
    auto obj1 = Dummy<short, int, long>{};
    auto obj2 = Dummy<short, int>{};
    auto obj3 = Dummy<short>{};
 // auto obj4 = Dummy<>{}; 
 // ERR: Too few template arguments for class template 'Dummy'
}
