#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct sNode {
    void        * payload;
    struct sNode* next;
} Node;
typedef void (*ElemApplyFn)(void*) ; 

Node* node_create(void* payload, Node* next) {
    Node* this = (Node*) calloc(1, sizeof(Node));
    this->payload = payload;
    this->next    = next;
    return this;
}

void node_destroy(Node* this, ElemApplyFn elemDestroyFn) {
    if (this->next) node_destroy(this->next, elemDestroyFn);
    elemDestroyFn(this->payload);
    free(this);
}

void node_print(Node* this, ElemApplyFn elemPrintFn) {
    elemPrintFn(this->payload);
    if (this->next) node_print(this->next, elemPrintFn);
}

typedef struct {
    char* name;
    unsigned age;
} Person;

Person* person_create(char* name, unsigned age) {
    Person* this = (Person*) calloc(1, sizeof(Person));
    this->name = strdup(name);
    this->age = age;
    return this;
}

void person_destroy(Person* this) {
    free(this->name);
    free(this);
}

void person_print(Person* this) {
    printf("Person<%s, %d>\n", this->name, this->age);
}

int main() {
    Node* persons = NULL;
    persons = node_create((void*)person_create("Carin", 53), persons);
    persons = node_create((void*)person_create("Berit", 42), persons);
    persons = node_create((void*)person_create("Anna", 31), persons);
    node_print(persons, (ElemApplyFn) &person_print);
    node_destroy(persons, (ElemApplyFn) &person_destroy);
}
