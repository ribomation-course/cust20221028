
template<bool value_>
struct BoolType {
    static constexpr bool value = value_;
    constexpr operator bool() const { return value; }
};


struct FalseType : BoolType<false> {};
struct TrueType : BoolType<true> {};

template<typename T1, typename T2>
struct EQ : FalseType {};

template<typename T>
struct EQ<T,T> : TrueType {};


template<typename T>
struct TypeIdentity { using type = T; };

template<typename...>
using void_t = void;



template<typename T, typename = void>
struct add_lvalue_ref_s : TypeIdentity<T> {};

template<typename T>
struct add_lvalue_ref_s<T, void_t<T&>> : TypeIdentity<T&> {};

template<typename T>
using add_lvalue_ref = typename add_lvalue_ref_s<T>::type;

static_assert(EQ<void,  add_lvalue_ref<void>>{});
static_assert(EQ<int&,  add_lvalue_ref<int>>{});






int main() {
}
