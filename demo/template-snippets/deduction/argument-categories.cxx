#include <iostream>
//#define COUNTER_VERBOSE
#include "invocation-counter.hxx"
using namespace std;

void fn(CountWrapper& cnt) {
    cout << __PRETTY_FUNCTION__ << ": " << cnt.counters << "\n";
}

void fn(CountWrapper const& cnt) {
    cout << __PRETTY_FUNCTION__ << ": " << cnt.counters << "\n";
}

void fn(CountWrapper&& cnt) {
    cout << __PRETTY_FUNCTION__ << ": " << cnt.counters << "\n";
}

void fn(CountWrapper const&& cnt) {
    cout << __PRETTY_FUNCTION__ << ": " << cnt.counters << "\n";
}

template<typename T>
void fn(T&& cnt) {
    cout << __PRETTY_FUNCTION__ << ": " << cnt.counters << "\n";
}

template<typename T>
void fn(T const&& cnt) {
    cout << __PRETTY_FUNCTION__ << ": " << cnt.counters << "\n";
}

auto create() {
    return CountWrapper{};
}

int main() {
    {
//        auto const obj = CountWrapper{};
        fn( create() );
    }
    CountWrapper::print_and_clear();
}
