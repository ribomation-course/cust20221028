#include <iostream>
#include <iomanip>
#include <boost/type_index.hpp>
using std::cout;
using std::setw;
using std::left;
namespace B = boost::typeindex;

template<typename T>
void func(T&& p) {
    cout << setw(40) << left
         << __PRETTY_FUNCTION__
         << "type(p)="
         << setw(20) << left
         << B::type_id_with_cvr<decltype(p)>().pretty_name()
         << "T="
         << B::type_id_with_cvr<T>().pretty_name()
         << "\n";
}

int main() {
    int         i    = 42;
    int const   ic   = 13;
    int      &  ir   = i;
    int const&  icr  = i;
    int      && irr  = i + 3;
    int const&& icrr = i + 7;
//std::forward<int>(17);
//std::move(i);
    func(17);
    func(i);
    func(ic);
    func(ir);
    func(icr);
    func(irr);
    func(icrr);
}






