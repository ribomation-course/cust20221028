#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::string;

template<typename T>
void useCase(T const& obj) {
   typename T::difference_type  iter(T::npos);
   cout << obj.size() << ", " << iter << endl;
   //...rest of func...
}

int main() {
    auto str = string{"hello"};
    useCase(str);
}

