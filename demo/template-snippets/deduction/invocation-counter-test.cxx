#include <iostream>
//#define COUNTER_VERBOSE
#include "invocation-counter.hxx"

using namespace std;

int main() {
    {
        auto obj = CountWrapper{};
    }
    CountWrapper::print_and_clear();
    {
        auto obj1 = CountWrapper{};
        auto obj2 = CountWrapper{obj1};
    }
    CountWrapper::print_and_clear();
    {
        auto obj1 = CountWrapper{};
        auto obj2 = CountWrapper{};
        obj2 = obj1;
    }
    CountWrapper::print_and_clear();
    {
        auto obj1 = CountWrapper{};
        auto obj2 = CountWrapper{};
        obj2 = std::move(obj1);
    }
    CountWrapper::print_and_clear();
}
