#include <iostream>
using std::cout;
using std::endl;

void first(int n) {
    cout << "first(" << n << ")\n";
}

template<typename T>
void second(T n) {
    cout << "second(" << n << ")\n";
}

template<typename T>
void useCase(T x) {
    first(42);
    second(x);
}

int main() {
    useCase(10);
}

