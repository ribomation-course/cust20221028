#include <iostream>

using std::cout;

template<typename T>
struct isPointer {
    static constexpr bool value = false;
};
template<typename T>
struct isPointer<T*> {
    static constexpr bool value = true;
};

template<typename T>
struct RemovePointer {
    using type = T;
};
template<typename T>
struct RemovePointer<T*> {
    using type = T;
};


template<typename T>
struct isFloatingPoint {
    static constexpr bool value = false;
};
template<>
struct isFloatingPoint<float> {
    static constexpr bool value = true;
};
template<>
struct isFloatingPoint<double> {
    static constexpr bool value = true;
};
template<>
struct isFloatingPoint<long double> {
    static constexpr bool value = true;
};


template<typename T>
void func(T arg) {
    cout << "typeid(T)=" << typeid(T).name() << ": ";

    using RawType = typename RemovePointer<T>::type;
    if constexpr(isPointer<T>::value && isFloatingPoint<RawType>::value) {
        cout << "T *  (" << typeid(RawType).name() << ")\n";
    }

    cout << "\n";
}

int main() {
    int       i  = 42;
    int const ic = 17;
    int      * ip   = &i;
    int const* icp  = &i;
    int      & ir   = i;
    int const& icr  = i;
    int      && irr = i + 3;
    int const& icrr = i + 5;

    func(i);
    func(ic);
    func(ip);
    func(icp);
    func(ir);
    func(icr);
    func(irr);
    func(icrr);

    cout << "----\n";
    float  f = 3.14;
    double d = 2 * f;
    func(f);
    func(d);
    func(&f);
    func(&d);
}

