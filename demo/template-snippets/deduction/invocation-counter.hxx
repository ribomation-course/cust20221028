#pragma once

#include <iosfwd>

struct Counters {
    unsigned create_count  = 0;
    unsigned destroy_count = 0;
    unsigned copy_count    = 0;
    unsigned move_count    = 0;

    void clear() {
        create_count  = 0;
        destroy_count = 0;
        copy_count    = 0;
        move_count    = 0;
    }
};

bool operator==(Counters const& lhs, Counters const& rhs) {
    return (lhs.create_count == rhs.create_count)
           && (lhs.destroy_count == rhs.destroy_count)
           && (lhs.copy_count == rhs.copy_count)
           && (lhs.move_count == rhs.move_count);
}

auto operator<<(std::ostream& os, Counters const& cnt) -> std::ostream& {
    return os << "{"
              << "creates=" << cnt.create_count
              << ", copies=" << cnt.copy_count
              << ", moves=" << cnt.move_count
              << ", destroys=" << cnt.destroy_count
              << ", diff=" << (cnt.create_count + cnt.copy_count + cnt.move_count - cnt.destroy_count)
              << "}";
}

inline void cnt_log(char const* msg, void const* addr) {
#ifdef COUNTER_VERBOSE
    static unsigned cnt = 0;
    std::cout << ++cnt << "] " << msg << " @ " << addr << "\n";
#endif
}

struct CountWrapper {
    inline static Counters counters;

    CountWrapper() {
        ++counters.create_count;
        cnt_log("CountWrapper()", this);
    }

    ~CountWrapper() {
        ++counters.destroy_count;
        cnt_log("~CountWrapper()", this);
    }

    CountWrapper(CountWrapper const&) {
        ++counters.copy_count;
        cnt_log("CountWrapper(CountWrapper const&)", this);
    }

    auto operator=(CountWrapper const&) -> CountWrapper& {
        ++counters.copy_count;
        cnt_log("operator=(CountWrapper const&)", this);
        return *this;
    }

    CountWrapper(CountWrapper&&) noexcept {
        ++counters.move_count;
        cnt_log("CountWrapper(CountWrapper&&)", this);
    }

    auto operator=(CountWrapper&&) noexcept -> CountWrapper& {
        ++counters.move_count;
        cnt_log("operator=(CountWrapper&&)", this);
        return *this;
    }

    static void clear() {
        counters.clear();
    }
    
    static void print_and_clear() {
        std::cout << counters << "\n";
        counters.clear();
    }
};

