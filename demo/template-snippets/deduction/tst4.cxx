#include <iostream>
using std::cout;

template<typename T>
struct FuncWrapper {
    void operator()(T x) {
        cout << "FuncWrapper<T>::operator()<T> " << x << "\n";
    }
};

template<>
struct FuncWrapper<int*> {
    void operator()(int* x) {
        cout << "FuncWrapper<int*>::operator()<int*> " << x << "\n";
    }
};

template<typename T>
struct FuncWrapper<T*> {
    void operator()(T x) {
        cout << "FuncWrapper<T*>::operator()<T*> " << x << "\n";
    }
};

template<typename T>
void func(T x) { 
    FuncWrapper<T>{}(x);
}

int main() {
    int* ptr = nullptr;
    func(ptr);
}

