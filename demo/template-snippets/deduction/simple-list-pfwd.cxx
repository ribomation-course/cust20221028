#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <memory>
#include <random>
#include "invocation-counter.hxx"


template<typename PayloadType>
class List {
    struct Link {
        PayloadType payload;
        Link* next;
        Link(PayloadType payload = PayloadType{}, Link* next = nullptr)
                : payload(std::move(payload)), next(next) {}
    };

    static void destroy(Link* node) {
        if (node->next) destroy(node->next);
        
        node->next = nullptr;
        node->~Link();
        
        auto alloc = std::allocator<Link>{};
        alloc.deallocate(node, 1);
    }

    Link* first;
public:
    List() {
        first = nullptr;
    }

    ~List() {
        if (size() > 0) {
            std::cerr << "list not empty\n";
            std::terminate();
        }
    }

    bool empty() const { return first == nullptr; }

    size_t size() const {
        auto      count = size_t{};
        for (auto node  = first; node != nullptr; node = node->next) ++count;
        return count;
    }

    void insert_first(PayloadType payload) {
        auto alloc         = std::allocator<Link>{};
        auto node          = alloc.allocate(1);
        first = new (node) Link{std::move(payload), first};
    }

    template<typename... ConstructorTypes>
    void emplace_first(ConstructorTypes... args) {
        auto alloc         = std::allocator<Link>{};
        auto node          = alloc.allocate(1);
        
        auto payloadMemBlk = &node->payload;
        new (payloadMemBlk) PayloadType{ std::forward<ConstructorTypes>(args)... };
       
        node->next = first;
        first = node;
    }

    PayloadType remove_first() {
        if (empty()) throw std::underflow_error{"empty list"};
        
        auto node = first;
        first = node->next;
        
        auto payload = std::move(node->payload);
        node->next = nullptr;
        node->~Link();

        auto alloc = std::allocator<Link>{};
        alloc.deallocate(node, 1);

        return payload;
    }

    void clear() {
        destroy(first);
        first = nullptr;
    }
};


struct Account {
    std::string  accno;
    int          balance;
    float        rate;
    CountWrapper cnt;

    Account() = default;
    Account(std::string const& accno, int balance, float rate)
            : accno(accno), balance(balance), rate(rate) {}
    Account(Account&&) noexcept = default;
    auto operator=(Account&&) noexcept -> Account& = default;
};

auto operator<<(std::ostream& os, Account const& a) -> std::ostream& {
    return os << "Account{" << a.accno
              << ", " << a.balance << " kr, "
              << std::setprecision(2) << a.rate << "%}";
}

int main(int argc, char** argv) {
    using std::cout;
    auto const N      = argc > 1 ? std::stoi(argv[1]) : 10;
    auto       r      = std::random_device{};
    auto       nxtLet = std::uniform_int_distribution<char>{'A', 'Z'};
    auto       nxtDig = std::uniform_int_distribution<char>{'0', '9'};
    auto       nxtBal = std::normal_distribution<float>{500, 100};
    auto       nxtRat = std::uniform_real_distribution<float>{1.F, 5.F};

    {
        cout << "-- count wrappers --\n";
        auto      lst = List<CountWrapper>{};
        for (auto k   = 0; k < N; ++k) {
            lst.insert_first(CountWrapper{});
        }
        cout << CountWrapper::counters << "\n";
        lst.clear();
    }
    CountWrapper::print_and_clear();

    {
        cout << "-- account --\n";
        auto      lst = List<Account>{};
        for (auto k   = 0; k < N; ++k) {
            auto accno   = std::string{}
                           + nxtLet(r) + nxtLet(r) + nxtLet(r)
                           + nxtDig(r) + nxtDig(r) + nxtDig(r) + nxtDig(r);
            auto balance = static_cast<int>(nxtBal(r));
            auto rate    = nxtRat(r);
            lst.insert_first(Account{accno, balance, rate});
        }
        cout << "lst.size=" << lst.size() << "\n";
        cout << "count: " << CountWrapper::counters << "\n";
        while (!lst.empty()) cout << lst.remove_first() << "\n";
        cout << "count: " << CountWrapper::counters << "\n";
    }
    CountWrapper::clear();
    
    {
        cout << "-- account emplace --\n";
        auto      lst = List<Account>{};
        for (auto k   = 0; k < N; ++k) {
            auto accno   = std::string{}
                           + nxtLet(r) + nxtLet(r) + nxtLet(r)
                           + nxtDig(r) + nxtDig(r) + nxtDig(r) + nxtDig(r);
            auto balance = static_cast<int>(nxtBal(r));
            auto rate    = nxtRat(r);
            lst.emplace_first(accno, balance, rate);
        }
        cout << "lst.size=" << lst.size() << "\n";
        cout << "count: " << CountWrapper::counters << "\n";
        while (!lst.empty()) cout << lst.remove_first() << "\n";
        cout << "count: " << CountWrapper::counters << "\n";
    }
}
