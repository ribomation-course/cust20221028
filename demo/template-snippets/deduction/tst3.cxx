#include <iostream>
using std::cout;

template<typename T>
void func(T x) { cout << "func(T) " << x << "\n"; }

template<typename T>
void func(T* x) { cout << "func(T*) " << x << "\n"; }

template<>
void func(int* x) { cout << "func(int*) " << x << "\n"; }

int main() {
    int* ptr = nullptr;
    func(ptr);
}

