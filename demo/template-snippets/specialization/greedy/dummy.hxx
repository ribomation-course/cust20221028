#pragma once

template<typename T>
class Dummy {
    T elem;
  public:
   Dummy() = default;
   T func(T);
};

template<typename T>
T Dummy<T>::func(T arg) {
    return 10 * arg;
}


