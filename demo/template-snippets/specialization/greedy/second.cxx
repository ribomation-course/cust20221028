#include <iostream>
#include "dummy.hxx"

void useCase2() {
    auto d = Dummy<int>{};
    std::cout << "[2nd] result=" << d.func(20) << std::endl;
}

