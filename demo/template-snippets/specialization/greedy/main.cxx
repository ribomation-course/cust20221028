#include <iostream>
#include "dummy.hxx"

void useCase1();
void useCase2();
void useCase3();

int main() {
    auto d = Dummy<int>{};
    std::cout << "[main] result=" << d.func(42) << std::endl;
    useCase1();
    useCase2();
    useCase3();
}

