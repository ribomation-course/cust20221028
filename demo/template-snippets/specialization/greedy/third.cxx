#include <iostream>
#include "dummy.hxx"

void useCase3() {
    auto d = Dummy<int>{};
    std::cout << "[3rd] result=" << d.func(30) << std::endl;
}

