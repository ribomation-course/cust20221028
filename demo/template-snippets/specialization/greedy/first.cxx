#include <iostream>
#include "dummy.hxx"

void useCase1() {
    auto d = Dummy<int>{};
    std::cout << "[1st] result=" << d.func(10) << std::endl;
}

