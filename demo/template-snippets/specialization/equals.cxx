#include <iostream>
#include <string>
#include <cstring>
#include <cmath>

template<typename T>
bool equals(T lhs, T rhs) {
    std::cout << __PRETTY_FUNCTION__ << ": ";
    return lhs == rhs;
}

template<>
bool equals(const char* lhs, const char* rhs) {
    std::cout << __PRETTY_FUNCTION__ << ": ";
    return strcmp(lhs, rhs) == 0;
}

template<>
bool equals(double lhs, double rhs) {
    std::cout << __PRETTY_FUNCTION__ << ": ";
    return std::abs(lhs - rhs) < 1E-6;
}

template<>
bool equals(float lhs, float rhs) {
    std::cout << __PRETTY_FUNCTION__ << ": ";
    return std::abs(lhs - rhs) < 1E-3;
}
template<>
bool equals(long double lhs, long double rhs) {
    std::cout << __PRETTY_FUNCTION__ << ": ";
    return std::abs(lhs - rhs) < 1E-9;
}

using std::cout;
using std::endl;
using namespace std::string_literals;

int main() {
    cout << std::boolalpha;
    cout << "equals<int> = "         << equals(42, 21 << 1) << endl;
    cout << "equals<double> = "      << equals(M_PI, 4 * atan(1)) << endl;
    cout << "equals<string> = "      << equals("HiFi"s, "Hi"s + "Fi"s) << endl;
    cout << "equals<const char*> = " << equals("Hello", "Hello") << endl;
    cout << "equals<const char*> = " << equals("Hello", "Hi") << endl;
}


