#include <iostream>
#include <cstring>
using namespace std;

template<typename PayloadType>
struct Node {
    PayloadType payload;
    Node* next;
    Node(PayloadType payload_, Node* next_) 
        : payload(payload_), next(next_) {}
    ~Node() {
        cout << "~Node<T>() @ " << this << endl;
    }
};

template<typename PayloadType>
struct Node<PayloadType*> {
    PayloadType* payloadPtr;
    Node * next;
    Node(PayloadType* payloadPtr_, Node* next_) {
        payloadPtr = new PayloadType{*payloadPtr_}; //clone
        next       = next_;
    }
    ~Node() {
        cout << "~Node<T*>() @ " << this << endl;
        delete payloadPtr;
    }
};

template<>
struct Node<char*> {
    char* payload;
    Node* next;
    Node(char* s, Node* next_) {
        payload = strdup(s); //uses malloc
        next    = next_;
    }
    ~Node() {
        cout << "~Node<char*>() @ " << this << endl;
        free(payload);
    }
};

template<typename T>
void dispose(Node<T>* node) {
    if (node->next != nullptr) dispose(node->next);
    delete node;
}

int main() {
    auto const N = 5;

    {
        Node<unsigned>* head = nullptr;
        for (auto k = 0; k < N; ++k) {
            head = new Node<unsigned>{k + 1U, head};
        }
        for (auto n = head; n != nullptr; n = n->next) {
            cout << n->payload << " ";
        }
        cout << endl;
        dispose(head);
    }

    {
        Node<int*>* head = nullptr;
        for (auto k      = 0; k < N; ++k) {
            auto value = (k + 1) * 10;
            head = new Node<int*>{&value, head};
        }
        for (auto n      = head; n != nullptr; n = n->next) {
            cout << *(n->payloadPtr) << " ";
        }
        cout << endl;
        dispose(head);
    }

    {
        Node<char*>* head = nullptr;
        const char* words[] = {"C++", "from", "Hello"};
        int numWords        = sizeof(words) / sizeof(*words);
        for (auto k = 0; k < numWords; ++k) {
            char* w = const_cast<char*>(words[k]);
            head = new Node<char*>{w, head};
        }
        for (auto n = head; n != nullptr; n = n->next) {
            cout << n->payload << " ";
        }
        cout << endl;
        dispose(head);
    }
}


