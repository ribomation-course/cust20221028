#include <iostream>
#include <cmath>
using namespace std;

template<typename T>
T mulBy2(T arg, unsigned exponent = 1) {
    cout << "<" << __PRETTY_FUNCTION__ << "> --> ";
    return arg * ::pow(2, exponent);
}

template<>
unsigned mulBy2(unsigned arg, unsigned exponent) {
    cout << "!" << __PRETTY_FUNCTION__ << "! --> ";
    return arg << exponent;
}


int main() {
    cout << "10*2 = "   << mulBy2(10) << endl;
    cout << "10*2^3 = " << mulBy2(10, 3) << endl;
    cout << "PI*2^3 = " << mulBy2(3.1415, 3) << endl;
    cout << "10*2^3 = " << mulBy2(10U, 3) << endl;
}

