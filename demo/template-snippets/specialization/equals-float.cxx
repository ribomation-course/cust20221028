#include <iostream>
using namespace std;

template<typename T> constexpr T tolerance              = 0;
template<> constexpr float       tolerance<float>       = 1E-3;
template<> constexpr double      tolerance<double>      = 1E-6;
//template<> constexpr long double tolerance<long double> = 1E-9;
template<> constexpr auto tolerance<long double> = 1E-9;

template<typename T>
bool equals(T lhs, T rhs) {
    static_assert(is_floating_point_v<T>, "must be floating-point");
    std::cout << __PRETTY_FUNCTION__ << " (" << tolerance<T> << ") --> ";
    return std::abs(lhs - rhs) < tolerance<T>;
}


int main() {
    cout << boolalpha;
    cout << "float      : " << equals(42.F, 44.F - 2.F) << endl;
    cout << "double     : " << equals(42., 44. - 2.) << endl;
    cout << "long double: " << equals(42.L, 44.L - 2.) << endl;
  //  cout << "int        : " << equals(42, 44 - 2) << endl;
}

