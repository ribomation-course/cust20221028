#include <iostream>
#include <string>
using namespace std;

template<typename FirstType, typename SecondType>
class Pair {
    FirstType first;
    SecondType second;
  public:
    Pair(FirstType first, SecondType second) : first(first), second(second) {}
    FirstType getFirst() const { return first; }
    SecondType getSecond() const { return second; }
};

template<typename T, typename U>
auto operator<<(ostream& os, Pair<T, U> const pair) -> ostream& {
    return os << "<" << pair.getFirst() << ", " << pair.getSecond() << ">";
}


int main() {
    auto office = Pair<double, double>{59.33605998947163, 18.07447753952352};
    cout << "office: " << office << endl;

    auto wordFreq = Pair<string, unsigned>{"Hamlet"s, 113U};
    cout << "count : " << wordFreq << endl;

    auto pack = Pair<short, short>{0xAB, 0xCD};
    cout << "pack : " << hex << pack << ", size=" << sizeof(pack) << endl;
}

