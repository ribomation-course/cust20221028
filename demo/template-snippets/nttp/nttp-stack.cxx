#include <iostream>
using namespace std;

template<typename ElemType, unsigned CAPACITY>
class Stack {
    ElemType storage[CAPACITY]{};
    int      idx = 0;
public:
    void     push(ElemType x) { storage[idx++] = x; }
    ElemType pop()            { return storage[--idx]; }
    bool empty() const { return idx == 0; }
    bool full()  const { return idx == CAPACITY; }
    Stack& operator =(Stack const& rhs) {
        for (auto k = 0U; k < CAPACITY; ++k) storage[k] = rhs.storage[k];
        idx = rhs.idx;
        return *this;
    }
};


template<typename T, unsigned N, typename Function>
void populate(Stack<T, N>& stk, Function fn) {
    for (short k = 1; !stk.full(); ++k) stk.push(fn(k));
}

template<typename T, unsigned N>
void drain(Stack<T, N>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}

int main() {
    {
        auto stk = Stack<int, 15>{};
        populate(stk, [](auto k) { return k * 10; });
        
        auto stk2 = Stack<int, 15>{};
        stk2 = stk;
        drain(stk);
        drain(stk2);
    }
}



