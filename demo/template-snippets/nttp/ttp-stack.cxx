#include <iostream>
#include <vector>
#include <deque>
#include <list>
using namespace std;

template<typename ElemType,
        template<typename T, typename A = std::allocator<T>> class Container>
class Stack {
    Container<ElemType> container{};
public:
    void push(ElemType x) {
        container.push_back(x);
    }
    ElemType pop() {
        ElemType x = container.back();
        container.pop_back();
        return x;
    }
    auto length() const { return container.size(); }
    bool empty()  const { return container.empty(); }
    Stack& operator=(Stack const& rhs) {
        container = rhs.container;
        return *this;
    }
};


template<typename T, template<typename,typename> class C, typename Function>
void populate(Stack<T, C>& stk, unsigned n, Function fn) {
    for (auto k = 1U; k <= n; ++k) stk.push(fn(k));
}

template<typename T, template<typename,typename> class C>
void drain(Stack<T, C>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}

int main() {
    {
        auto stk = Stack<long, vector>{};
        populate(stk, 10, [](auto k) { return k * 10; });
        drain(stk);
    }
    {
        auto stk = Stack<long double, deque>{};
        populate(stk, 6, [](auto k) { return k * 3.1415926; });
        drain(stk);
    }
    {
        auto stk = Stack<int, list>{};
        populate(stk, 10, [](auto k) { return k * 42; });
        
        auto stk2 = Stack<int, list>{};
        stk2 = stk;
        drain(stk);
        drain(stk2);
    }
}



