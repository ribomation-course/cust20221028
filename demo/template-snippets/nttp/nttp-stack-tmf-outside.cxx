#include <iostream>
using namespace std;

template<typename ElemType, unsigned CAPACITY>
class Stack {
    ElemType storage[CAPACITY]{};
    int      idx = 0;
public:
    void     push(ElemType x) { storage[idx++] = x; }
    ElemType pop()            { return storage[--idx]; }
    bool empty() const { return idx == 0; }
    bool full()  const { return idx == CAPACITY; }

    template<typename U, unsigned N>
    Stack& operator =(Stack<U,N> const& rhs);
    
    template<typename, unsigned> friend class Stack;
};

template<typename ElemType, unsigned CAPACITY>
    template<typename U, unsigned N>
auto Stack<ElemType,CAPACITY>::operator =(Stack<U,N> const& rhs) -> Stack& {
    auto numElems = ::min(CAPACITY, N);
    for (auto k = 0U; k < numElems; ++k) storage[k] = rhs.storage[k];
    idx = numElems;
    return *this;
}



template<typename T, unsigned N, typename Function>
void populate(Stack<T, N>& stk, Function fn) {
    for (short k = 1; !stk.full(); ++k) stk.push(fn(k));
}

template<typename T, unsigned N>
void drain(Stack<T, N>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}

int main() {
    auto stk = Stack<int, 15>{};
    populate(stk, [](auto k) { return k * 10; });

    auto stk2 = Stack<int, 10>{};
    stk2 = stk;
    drain(stk);
    drain(stk2);
}



