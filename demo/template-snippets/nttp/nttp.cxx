#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
using namespace std;

template<char sep = ';', typename Iterator>
string csv(Iterator it, Iterator last) {
    auto      csv   = string{};
    for (bool delim = false; it != last; ++it, delim = true) {
        if (delim) csv += sep;
        csv += to_string(*it);
    }
    return csv;
}

template<typename T, T rhs = T{}>
bool divisible_by(T lhs) { return (lhs % rhs) == 0; }


int main() {
    int  numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12};
    auto N         = sizeof(numbers) / sizeof(*numbers);
    cout << "CSV: " << csv(numbers, numbers + N) << endl;
    cout << "CSV: " << csv<'/'>(numbers, numbers + N) << endl;

    copy_if(numbers, numbers + N, 
            ostream_iterator<int>(cout, " "), 
            divisible_by<int,3>);
}

