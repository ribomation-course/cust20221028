#include <iostream>

using namespace std;

template<typename ElemType, auto CAPACITY>
class Stack {
    using size_type = decltype(CAPACITY);
    ElemType  storage[CAPACITY]{};
    size_type idx{};
public:
    void     push(ElemType x) { storage[idx++] = x; }
    ElemType pop() { return storage[--idx]; }
    
    size_type length() const { return idx; }
    bool      empty()  const { return length() == 0; }
    bool      full()   const { return length() == CAPACITY; }

    Stack& operator=(Stack const& rhs) {
        for (auto k = 0U; k < CAPACITY; ++k) storage[k] = rhs.storage[k];
        idx = rhs.idx;
        return *this;
    }
};


template<typename T, auto N, typename Function>
void populate(Stack<T, N>& stk, Function fn) {
    for (short k = 1; !stk.full(); ++k) stk.push(fn(k));
}

template<typename T, auto N>
void drain(Stack<T, N>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}

int main() {
    {
        auto stk = Stack<int, 10UL>{};
        populate(stk, [](auto k) { return k * 10'000; });
        drain(stk);
    }
    {
        auto stk = Stack<long double, 8>{};
        populate(stk, [](auto k) { return k * 3.1415; });
        drain(stk);
    }
}



