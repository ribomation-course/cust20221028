#include <iostream>
using namespace std;

template<typename ElemType, unsigned CAPACITY>
class Stack {
    ElemType storage[CAPACITY]{};
    int      idx = 0;
public:
    void     push(ElemType x);
    ElemType pop();
    bool     empty() const;
    bool     full() const;
    Stack&   operator=(Stack const& rhs);
};

template<typename T, unsigned N>
void Stack<T,N>::push(T x) { storage[idx++] = x; }

template<typename T, unsigned N>
T Stack<T,N>::pop() { return storage[--idx]; }

template<typename T, unsigned N>
bool Stack<T,N>::empty() const { return idx == 0; }

template<typename T, unsigned N>
bool Stack<T,N>::full() const { return idx == N; }

//template<typename T, unsigned N>
//Stack<T,N>& Stack<T,N>::operator=(Stack const& rhs) {
//    for (auto k = 0U; k < N; ++k) storage[k] = rhs.storage[k];
//    top = rhs.top;
//    return *this;
//}

template<typename T, unsigned N>
auto Stack<T,N>::operator=(Stack const& rhs) -> Stack& {
    for (auto k = 0U; k < N; ++k) storage[k] = rhs.storage[k];
    idx = rhs.idx;
    return *this;
}



template<typename T, unsigned N, typename Function>
void populate(Stack<T, N>& stk, Function fn) {
    for (short k = 1; !stk.full(); ++k) stk.push(fn(k));
}

template<typename T, unsigned N>
void drain(Stack<T, N>& stk) {
    while (!stk.empty()) cout << stk.pop() << " ";
    cout << endl;
}

int main() {
    auto stk  = Stack<int, 15>{};
    populate(stk, [](auto k) { return k * 10; });
    auto stk2 = Stack<int, 15>{};
    stk2 = stk;
    drain(stk);
    drain(stk2);
}



