#include <iostream>

using namespace std;

template<const char* MSG>
void func() {
    cout << "[func] " << MSG << endl;
}

const char msg[] = "This works too";
int main() {
    func<msg>();
}


