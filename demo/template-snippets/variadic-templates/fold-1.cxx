#include <iostream>
#include <string>
using namespace std;

template<typename... Args>
auto sum(Args... args) {
    return (... + args);
}

template<typename... Args>
auto product(Args... args) {
    return (1 * ... * args);
}

int main() {
    cout << "sum(1..5) = " << sum(1,2,3,4,5) << endl;
    cout << "str       = " << sum("Hi"s, "-"s, "Fi"s) << endl;
    cout << "5!        = " << product(2,3,4,5) << endl;
}



