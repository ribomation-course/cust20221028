#include <iostream>
#include <string>
using namespace std;

template<typename... Args>
void print(Args... args) {
    cout << __PRETTY_FUNCTION__ << endl;
    (cout << ... << args) << endl;
}

int main() {
    cout << boolalpha;
    print(42, 3.1415, true, "Howdy");
}



