#include <iostream>
using namespace std;

void print() {
    cout << __PRETTY_FUNCTION__ << endl;
}

template<typename HeadType, typename... TailTypes>
void print(HeadType head, TailTypes... tail) {
    cout << __PRETTY_FUNCTION__ << " --> " << head << endl;
    print(tail...);
}

int main() {
    cout << boolalpha;
    print(42, 3.1415, true, "Howdy");
}


