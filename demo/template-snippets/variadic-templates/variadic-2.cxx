#include <iostream>
using namespace std;

template<typename... TailTypes>
void count(TailTypes... tail) {
    cout << __PRETTY_FUNCTION__
         << ": #types=" << sizeof...(TailTypes)
         << ", #args=" << sizeof...(tail)
         << endl;
}

int main() {
    cout << boolalpha;
    count(42, 3.1415, true, "Howdy");
    count(17);
    count();
}


