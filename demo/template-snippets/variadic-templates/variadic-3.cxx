#include <iostream>
using namespace std;

template<typename HeadType, typename... TailTypes>
void print(HeadType head, TailTypes... tail) {
    cout << __PRETTY_FUNCTION__ << " --> " << head << endl;
    if constexpr(sizeof...(tail) > 0) {
        print(tail...);
    }
}

int main() {
    cout << boolalpha;
    print(42, 3.1415, true, "Howdy");
}


