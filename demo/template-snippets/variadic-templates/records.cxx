#include <iostream>
#include <string>
using namespace std;

//forward decl of type
template<typename... Types>
class Record;

//recursive def
template<typename Head, typename... Tail>
class Record<Head, Tail...> {
    Head            head;
    Record<Tail...> tail;
public:
    Record() {}
    Record(Head const& head_, Record<Tail...> const& tail)
            : head(head_), tail(tail) {}
    Record(Head const& head_, Tail const&... tail_)
            : head(head_), tail(tail_...) {}

    Head& getHead() { return head; }
    Head const& getHead() const { return head; }

    Record<Tail...>& getTail() { return tail; }
    Record<Tail...> const& getTail() const { return tail; }
    
    unsigned size() const { return 1 + sizeof...(Tail); }
    string typesOf() const { 
        auto typeOf = [](Head elem) {
            auto t = static_cast<string>(typeid(elem).name());
            if (t == "b"s) return "bool"s; 
            if (t == "c"s) return "char"s; 
            if (t == "i"s) return "int"s; 
            if (t == "j"s) return "unsigned int"s; 
            if (t == "l"s) return "long"s; 
            if (t == "m"s) return "unsigned long"s; 
            if (t == "x"s) return "long long"s; 
            if (t == "y"s) return "unsigned long long"s; 
            if (t == "f"s) return "float"s; 
            if (t == "d"s) return "double"s; 
            if (t == "e"s) return "long double"s; 
            if (t == "PKc"s) return "const char*"s; 
            if (t.find("basic_string"s) != string::npos) return "std::string"s;
            return t;
        };
        return typeOf(head) + (tail.size() > 0 ? ", "s : ""s) + tail.typesOf();
    }
};

//base-case of recursive def
template<>
class Record<> {
public:
    unsigned size() const { return 0; }
    string typesOf() const { return ""s; }
};


//accessor type: recursive def
template<unsigned N>
struct Retrieve {
    template<typename Head, typename... Tail>
    static auto retrieve(Record<Head, Tail...> const& rec) {
        return Retrieve<N - 1>::retrieve(rec.getTail());
    }
};

//accessor type: base-case
template<>
struct Retrieve<0> {
    template<typename Head, typename... Tail>
    static Head const& retrieve(Record<Head, Tail...> const& rec) {
        return rec.getHead();
    }
};

//accessor fn
template<unsigned N, typename... Types>
auto get(Record<Types...> const& rec) {
    return Retrieve<N>::retrieve(rec);
}


//printer: base-case
auto printRecord(ostream& os, Record<> const& , bool firstElement = true) -> ostream& {
   return os << (firstElement ? "<" : ">");
}

//printer: recursive case
template<typename Head, typename... Tail>
auto printRecord(ostream& os, Record<Head,Tail...> const& rec, bool firstElement = true) -> ostream& {
    os << (firstElement ? "<" : ", ") << rec.getHead();
  return printRecord(os, rec.getTail(), false);
}

//printer: operator<<
template<typename... Types>
auto operator<<(ostream& os, Record<Types...> const& rec) -> ostream& {
    return printRecord(os, rec);
}


//factory fn
template<typename... Types>
auto make_record(Types... elems) {
    return Record<Types...>(elems...);
}


int main() {
    auto r = Record<int, float, string>{42, 3.1415F, "C++ is cool"s};
    cout << "get<N>(rec): " << get<0>(r) << " " << get<1>(r)<< " " << get<2>(r) << endl;
    cout << "operator<< : " << r << endl;
    
    auto r2 = make_record("C++ string"s, 10, 10U, 10L, 10UL, 10LL, 123ULL, 3.14F, 3.14, 2.71L, true, "C string", '#');
    cout << "r2         : " << r2 << endl;
    cout << "r2.size    : " << r2.size() << endl;
    cout << "r2.types   : " << r2.typesOf() << endl;
}


