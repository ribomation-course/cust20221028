#include <iostream>
#include <iomanip>
#include <string>
#include <random>
using namespace std;


template<typename ElemType, unsigned CAPACITY>
class Stack {
    ElemType storage[CAPACITY]{};
    int      idx = 0;
public:
    bool empty() const { return idx == 0; }
    bool full() const { return idx == CAPACITY; }
    ElemType pop() { return storage[--idx]; }
    ElemType& top() { return storage[idx]; }

    template<typename... ConstructorArgs>
    void push(ConstructorArgs... args) {
        auto memoryBlock = &storage[idx++];
        new (memoryBlock) ElemType{args...};
    }
};


class Account {
    string accno;
    int    balance;
    float  rate;
public:
    Account() = default;
    Account(string a, int b, float r)
            : accno{move(a)}, balance{b}, rate{r} {}
    string getAccno() const { return accno; }
    int update(int amount) {
        balance += amount * (1 + rate);
        return balance;
    }
    friend auto operator<<(ostream& os, Account const& a) -> ostream& {
        return os << "Account{" << a.accno
                  << ", SEK " << a.balance << ", "
                  << setprecision(2) << a.rate << "%}";
    }
};


int main() {
    auto r           = random_device{};
    auto nextLetter  = uniform_int_distribution<char>{'A', 'Z'};
    auto nextDigit   = uniform_int_distribution<char>{'0', '9'};
    auto nextBalance = normal_distribution<float>{500, 200};
    auto nextRate    = uniform_real_distribution<float>{0.1, 1.9};
    auto repeat      = [&r](unsigned n, uniform_int_distribution<char> gen) {
        auto result = ""s;
        while (n-- > 0) result += gen(r);
        return result;
    };
    
    auto stk = Stack<Account, 10>{};
    while (!stk.full()) {
        auto accno   = repeat(3, nextLetter) + repeat(6, nextDigit);
        auto balance = static_cast<int>(nextBalance(r));
        auto rate    = nextRate(r);
        
        stk.push(accno, balance, rate);
        stk.top().update(100);
    }
    while (!stk.empty()) cout << stk.pop() << endl;
}


