#include <iostream>
#include <string>

using namespace std;

template<typename T>
struct SeparatorWrapper {
    SeparatorWrapper(const char sep_, T const& arg_) : sep(sep_), arg(arg_) {}
    friend auto operator<<(ostream& os, SeparatorWrapper const& w) -> ostream& {
        return os << w.sep << w.arg;
    }
private:
    const char sep;
    T const& arg;
};

template<const char sep = ' ', typename Head, typename... Tail>
void print(Head head, Tail... tail) {
    cout << __PRETTY_FUNCTION__ << endl;
    if constexpr(sizeof...(tail) == 0) {
        cout << head << endl;
    } else if constexpr(sizeof...(tail) > 0) {
        cout << head;
        (cout << ... << SeparatorWrapper(sep, tail));
        cout << endl;
    }
}

int main() {
    cout << boolalpha;
    print(42, 3.1415, true, "Howdy");
    print<';'>(42, 3.1415, true, "Howdy");
    print("JustMe");
}



