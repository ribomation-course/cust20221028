#include <iostream>
#include <functional>
#include <algorithm>
#include <numeric>

using namespace std;
using namespace std::placeholders;

template<typename T, unsigned N>
void print(T(& arr)[N]) {
    for (auto k = 0U; k < N; ++k) cout << arr[k] << " ";
    cout << endl;
}

int main() {
    int numbers[10];
    iota(begin(numbers), end(numbers), 1);
    print(numbers);

    auto sum = reduce(begin(numbers), end(numbers));
    cout << "sum=" << sum << endl;

    auto prod = reduce(begin(numbers), end(numbers), 1, multiplies<>{});
    cout << "prod=" << prod << endl;

    auto divisible = [](auto x, auto n) { return x % n == 0; };

    auto cnt = count_if(begin(numbers), end(numbers), bind(divisible, _1, 3));
    cout << "cnt=" << cnt << endl;

    transform(begin(numbers), end(numbers), begin(numbers), bind(multiplies<>{}, _1, 10));
    print(numbers);
}
