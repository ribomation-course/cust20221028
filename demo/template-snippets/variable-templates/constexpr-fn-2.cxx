#include <iostream>
#include <iomanip>

using namespace std;

constexpr auto fibonacci(unsigned n) {
    auto f0 = 0UL;
    auto f1 = 1UL;
    while (--n > 0) {
        auto f = f0 + f1;
        f0 = f1;
        f1 = f;
    }
    return f1;
}

struct Pair {
    unsigned      arg;
    unsigned long res;
};

int main() {
    constexpr Pair pairs[] = {
            {5,  fibonacci(5)},
            {10, fibonacci(10)},
            {20, fibonacci(20)},
            {30, fibonacci(30)},
            {40, fibonacci(40)},
            {50, fibonacci(50)},
    };
    for (auto[arg, res]: pairs)
        cout << dec << "fib(" << arg << ") = " << res
             << hex << " (0x" << res << ")" << endl;
}


