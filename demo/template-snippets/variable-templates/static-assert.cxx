#include <iostream>
using namespace std;

template<typename T>
struct is_integer_type {
    static constexpr bool value = false;
};

template<>
struct is_integer_type<int> {
    static constexpr bool value = true;
};
template<>
struct is_integer_type<unsigned int> {
    static constexpr bool value = true;
};
template<>
struct is_integer_type<long> {
    static constexpr bool value = true;
};
template<>
struct is_integer_type<unsigned long> {
    static constexpr bool value = true;
};

template<typename T>
constexpr bool is_integer_type_v = is_integer_type<T>::value;


template<typename T>
T multBy2(T n) {
    static_assert(is_integer_type_v<T>, "Argument-type must be int or long");
    return n << 1;
}

int main() {
    cout << "21 * 2 = " << multBy2(21) << endl;
    cout << "21 * 2 = " << multBy2(21L) << endl;
    cout << "21 * 2 = " << multBy2(21U) << endl;
    cout << "21 * 2 = " << multBy2(21UL) << endl;
}


