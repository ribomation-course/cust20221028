#include <iostream>
#include <random>
using namespace std;

constexpr auto sum(unsigned n) {
    return n * (n + 1) / 2;
}

int main() {
    constexpr auto N = 10U;
    constexpr auto S = sum(N);
    cout << "sum(1.." << N << ") = " << S << endl;

    auto       r       = random_device{};
    auto       nextInt = uniform_int_distribution<>{10, 100};
    const auto n       = nextInt(r);
    const auto s       = sum(n);
    cout << "sum(1.." << n << ") = " << s << endl;
}


