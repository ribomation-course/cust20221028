#include <iostream>
#include <stdexcept>
#include <string>
#include <cstring>
#include <cmath>
#include <type_traits>
using namespace std;


template<typename T>
T abs(T x) { return x < 0 ? -x : x; }

template<typename T>
constexpr T error_tolerance = T(0.000'001);

template<typename T>
constexpr bool equals(T a, T b) {
    cout << __PRETTY_FUNCTION__ << " --> ";
    if constexpr (is_integral_v<T>) {
        return a == b;
    } else if constexpr (is_floating_point_v<T>) {
        return abs(a - b) < error_tolerance<T>;
    } else if constexpr (is_same_v<T, string>) {
        return a == b;
    } else if constexpr (is_convertible_v<T, const char*>) {
        auto A = static_cast<const char*>(a);
        auto B = static_cast<const char*>(b);
        return strcmp(A, B) == 0;
    } else {
        throw invalid_argument{"unsupported type: \""s
                               + typeid(T).name()
                               + "\" in fn \""s + __PRETTY_FUNCTION__ + "\""s};
    }
}

struct Dummy {
    int value;
    Dummy(int v) : value{v} {}
};

int main() {
    cout << boolalpha;
    cout << "42 == 21<<1    : " << equals(42U, 21U << 1) << endl;
    cout << "3.14 == PI     : " << equals(3.1415926L, 4.L * atan(1)) << endl;
    cout << "WiFi == Wi + Fi: " << equals("WiFi"s, "Wi"s + "Fi"s) << endl;
    cout << "10 == 11       : " << equals(10ULL, 11ULL) << endl;

    const char* t1  = "Hello";
    const char t2[] = {'H', 'e', 'l', 'l', 'o', '\0',};
    cout << "Hello == Hello : " << equals(t1, t2) << endl;

    cout << "Dummy          : " << equals(Dummy{10}, Dummy{2 * 5}) << endl;
}


