#include <iostream>
#include <iomanip>
using namespace std;

template<typename T>
constexpr T PI = T(3.1415926535897932385L);

template<typename T>
T area(T radius) {
    return PI<T> * radius * radius;
}

template<typename T>
T perimeter(T radius) {
    return PI<T> * radius * 2;
}


int main() {
    {
        float r = 10;
        cout << fixed << setprecision(4) 
        << "area=" << area(r) << ", perimeter=" << perimeter(r) << endl;
    }
    {
        double r = 1'000;
        cout << fixed << setprecision(8)
                << "area=" << area(r) << ", perimeter=" << perimeter(r) << endl;
    }
    {
        long double r = 1'000'000;
        cout << fixed << setprecision(16)
                << "area=" << area(r) << ", perimeter=" << perimeter(r) << endl;
    }
}
