#include <iostream>
#include <iomanip>
using namespace std;

template<size_t n>
constexpr size_t factorial = n * factorial<n - 1>;

template<>
constexpr size_t factorial<0> = 1;

int main() {
    size_t values[] = {
            factorial<5>,
            factorial<10>,
            factorial<15>,
            factorial<20>,
            factorial<25>,
    };
    for (auto f : values) cout << f << " ";
    cout << endl;
    for (auto f : values) cout << hex << f << " ";
    cout << endl;
}


