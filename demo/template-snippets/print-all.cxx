#include <iostream>
#include <string>
#include <functional>
#include <algorithm>
#include <numeric>
#include <vector>
#include <map>
#include <iterator>

using namespace std;

template<typename T, unsigned N>
void print(string const& name, const T(& arr)[N]) {
    cout << name << ": [";
    bool      first = true;
    for (auto k     = 0U; k < N; ++k) {
        if (first) first = false; else cout << ", ";
        cout << arr[k];
    }
    cout << "]" << endl;
}

template<typename T>
void print(string const& name, const T* arr, unsigned N) {
    cout << name << ": [";
    bool      first = true;
    for (auto k     = 0U; k < N; ++k) {
        if (first) first = false; else cout << ", ";
        cout << arr[k];
    }
    cout << "]" << endl;
}

template<typename KeyType, typename ValType>
auto operator<<(ostream& os, pair<KeyType, ValType> const& elem) -> ostream& {
    return os << elem.first << ":" << elem.second;
}

template<template<typename, typename...> class Container, typename E, typename... Args>
void print(string const& name, Container<E, Args...> const& container) {
    cout << name << ": [";
    bool first = true;
    for (auto const& elem: container) {
        if (first) first = false; else cout << ", ";
        cout << elem;
    }
    cout << "]" << endl;
}


template<typename E>
struct ArrayInsertIterator : iterator<output_iterator_tag, void, void, void, void> {
    ArrayInsertIterator(E* array_, unsigned size_) : arr{array_}, N{size_} {}

    void operator++()  { ++current; }
    auto operator*() -> ArrayInsertIterator& { return *this; }
    auto operator=(E value) -> ArrayInsertIterator& {
        arr[current] = value;
        return *this;
    }

private:
    E* const arr;
    const unsigned N;
    unsigned current = 0;
};

template<typename E>
auto array_inserter(E* arr, unsigned N) -> ArrayInsertIterator<E> {
    return {arr, N};
}

template<typename E, unsigned N>
auto array_inserter(E(& arr)[N]) -> ArrayInsertIterator<E> {
    return {arr, N};
}

int main() {
    auto const N      = 10;
    int        arr[N];
    auto       dynarr = new int[N];
    auto       vec    = vector<int>{};
    auto       tbl    = map<int, int>{};
    auto       gen    = [k = 0]() mutable { return ++k, k * k; };
    auto       gen2   = [k = 0]() mutable { return ++k, pair<int, int>(k, k * k); };

    generate_n(array_inserter(arr), N, gen);
    print("array  "s, arr);

    generate_n(array_inserter(dynarr, N), N, gen);
    print("pointer"s, dynarr, N);

    generate_n(back_inserter(vec), N, gen);
    print("vector "s, vec);

    generate_n(inserter(tbl, tbl.end()), N, gen2);
    print("map    "s, tbl);

}
