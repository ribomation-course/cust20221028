#include <iostream>
#include "my-stl.hxx"
using namespace std;
int main() {
    auto const N = 20U;
    int        numbers[N]{};
    generate(numbers, numbers + N, [next = 0]() mutable { return ++next, next; });
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; }); cout << endl;

    auto reducer = [](auto acc, auto n) { return acc + n*n; };
    using ReducerFn = int (int,int);
    auto x1 = reduce<long, int*, ReducerFn>(numbers, numbers + N, reducer);
    cout << "x1: " << x1 << endl;
    
    auto x2 = reduce<long, int*>(numbers, numbers + N, reducer);
    cout << "x2: " << x2 << endl;
    
    auto x3 = reduce<long>(numbers, numbers + N, reducer);
    cout << "x3: " << x3 << endl;
    
    auto x4 = reduce<>(numbers, numbers + N, reducer);
    cout << "x4: " << x4 << endl;
    
    auto x5 = reduce(numbers, numbers + N, reducer);
    cout << "x5: " << x5 << endl;
    
}



