#include <iostream>
#include <string>
using namespace std;

template<typename T>
T MAX(T a, T b) { return a >= b ? a : b; }

int main() {
    {
        int x = 10, y = 42;
        cout << "max("<<x<<", "<<y<<") = "<< MAX<int>(x,y) << endl;
    }
    {
        long x = 512L, y = 123L;
        cout << "max("<<x<<", "<<y<<") = "<< MAX<long>(x,y) << endl;
    }
    {
        float x = 2.71F, y = 3.14F;
        cout << "max("<<x<<", "<<y<<") = "<< MAX<float>(x,y) << endl;
    }
    {
        double x = 2.71, y = 3.14;
        cout << "max("<<x<<", "<<y<<") = "<< MAX<double>(x,y) << endl;
    }
    {
        string x = "abc"s, y = "ABC"s;
        cout << "max("<<x<<", "<<y<<") = "<< MAX<string>(x, y) << endl;
    }
}


