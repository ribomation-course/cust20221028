#include <iostream>
using namespace std;

template<typename T>
T MAX(T a, T b) { return a >= b ? a : b; }

int main() {
    signed   int x = -10;
    unsigned int y = +10;
    cout << "max(" << x << ", " << y << ") = " << MAX<int>(x, y) << endl;
}


