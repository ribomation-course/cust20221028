#include <iostream>
#include "my-stl.hxx"
using namespace std;
int main() {
    auto const N = 20U;
    int        numbers[N]{};
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; });
    cout << endl;

    generate(numbers, numbers + N, [next = 0]() mutable { return ++next, next; });
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; }); cout << endl;

    auto cnt = count_if(numbers, numbers + N, [](auto n) { return (n % 3) == 0; });
    cout << "cnt: " << cnt << endl;

    auto sum = reduce<int>(numbers, numbers + N, [](auto acc, auto n) { return acc + n; });
    cout << "sum: " << sum << endl;

    auto prd = reduce<long>(numbers, numbers + N, [](auto acc, auto n) { return acc * n; });
    cout << "prd: " << prd << endl;
    
    map(numbers, numbers + N, [](auto n) { return n * n; });
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; }); cout << endl;
}



