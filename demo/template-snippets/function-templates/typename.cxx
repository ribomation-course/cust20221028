#include <iostream>
using namespace std;

template<typename T> void func1(T t) {}
template<class T>    void func2(T t) {}
//template<struct T>   void func3(T t) {}

int main() {
    func1(42);
    func2(42);
//    func3(42);
    return 0;
}
