#pragma once

template<typename Iterator, typename Function>
void foreach(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) fn(*first);
}

template<typename Iterator, typename Function>
void generate(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) *first = fn();
}

template<typename Iterator, typename Function>
unsigned count_if(Iterator first, Iterator last, Function fn) {
    auto count = 0U;
    for (; first != last; ++first) if (fn(*first)) ++count;
    return count;
}

template<typename Iterator, typename Function>
void map(Iterator first, Iterator last, Function fn) {
    for (; first != last; ++first) *first = fn(*first);
}

template<typename RetType = long, typename Iterator, typename Function>
RetType reduce(Iterator first, Iterator last, Function fn) {
    RetType result = *first;
    for (++first; first != last; ++first) result = fn(result, *first);
    return result;
}

template<typename RetType, typename Iterator, typename MapFn, typename ReduceFn>
RetType map_reduce(Iterator first, Iterator last, MapFn mapFn, ReduceFn reduceFn) {
    RetType result = mapFn(*first);
    for (++first; first != last; ++first) result = reduceFn(result, mapFn(*first));
    return result;
}

