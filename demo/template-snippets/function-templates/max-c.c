extern int printf(const char*, ...);

int     max_i(int a   , int b)    { return a >= b ? a : b; }
long    max_l(long a  , long b)   { return a >= b ? a : b; }
float   max_f(float a , float b)  { return a >= b ? a : b; }
double  max_d(double a, double b) { return a >= b ? a : b; }

int main() {
    {
        int x = 10, y = 42;
        printf("max(%d, %d) = %d\n", x, y, max_i(x,y));
    }
    {
        long x = 512L, y = 123L;
        printf("max(%ld, %ld) = %ld\n", x, y, max_l(x,y));
    }
    {
        float x = 2.71F, y = 3.14F;
        printf("max(%.2f, %.2f) = %.2f\n", x, y, max_f(x,y));
    }
    {
        double x = 2.71, y = 3.14;
        printf("max(%.2f, %.2f) = %.2f\n", x, y, max_d(x,y));
    }
}

