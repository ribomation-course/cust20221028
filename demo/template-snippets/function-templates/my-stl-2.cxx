#include <iostream>
#include <vector>
#include "my-stl.hxx"
using namespace std;
int main() {
    auto const N = 20U;
    auto numbers = vector<int>(N);
    foreach(numbers.begin(), numbers.end(), [](auto n) { cout << n << " "; });
    cout << endl;

    generate(numbers.begin(), numbers.end(), [next = 0]() mutable { return ++next, next; });
    foreach(numbers.begin(), numbers.end(), [](auto n) { cout << n << " "; });
    cout << endl;

    auto cnt = count_if(numbers.begin(), numbers.end(), [](auto n) { return (n % 3) == 0; });
    cout << "cnt: " << cnt << endl;

    auto sum = reduce<int>(numbers.begin(), numbers.end(), [](auto acc, auto n) { return acc + n; });
    cout << "sum: " << sum << endl;

    auto prd = reduce<long>(numbers.begin(), numbers.end(), [](auto acc, auto n) { return acc * n; });
    cout << "prd: " << prd << endl;

    map(numbers.begin(), numbers.end(), [](auto n) { return n * n; });
    foreach(numbers.begin(), numbers.end(), [](auto n) { cout << n << " "; });
    cout << endl;
}



