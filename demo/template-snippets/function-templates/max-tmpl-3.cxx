#include <iostream>
#include <string>
using namespace std;

template<typename T>
T MAX(T a, T b) { 
    cout << "<" << __PRETTY_FUNCTION__ << "> ";
    return a >= b ? a : b; 
}

int main() {
    { int x = 10, y = 42;
        cout << "max("<<x<<", "<<y<<") = "<< MAX(x,y) << endl;
    }
    { long x = 512L, y = 123L;
        cout << "max("<<x<<", "<<y<<") = "<< MAX(x,y) << endl;
    }
    { float x = 2.71F, y = 3.14F;
        cout << "max("<<x<<", "<<y<<") = "<< MAX(x,y) << endl;
    }
    { double x = 2.71, y = 3.14;
        cout << "max("<<x<<", "<<y<<") = "<< MAX(x,y) << endl;
    }
    { string x = "abc"s, y = "ABC"s;
        cout << "max("<<x<<", "<<y<<") = "<< MAX(x, y) << endl;
    }
}


