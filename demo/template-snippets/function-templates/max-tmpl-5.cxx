#include <iostream>
using namespace std;

template<typename T>
T MAX(T a, T b) { return a >= b ? a : b; }

struct Dummy {
    int value;
    Dummy(int v) : value{v} {}
};
auto operator <<(ostream& os, Dummy const& d) -> ostream& {
    return os << d.value;
}
bool operator >=(Dummy const& lhs, Dummy const& rhs) {
    return lhs.value >= rhs.value;
}
int main() {
    auto x = Dummy{13};
    auto y = Dummy{17};
    cout << "max(" << x << ", " << y << ") = " << MAX(x, y) << endl;
}
