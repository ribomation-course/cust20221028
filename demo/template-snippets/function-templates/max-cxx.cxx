#include <iostream>
#include <string>
using namespace std;

int     max(int a   , int b)    { return a >= b ? a : b; }
long    max(long a  , long b)   { return a >= b ? a : b; }
float   max(float a , float b)  { return a >= b ? a : b; }
double  max(double a, double b) { return a >= b ? a : b; }
string  max(string a, string b) { return a >= b ? a : b; }
char    max(char a, char b)     { return a >= b ? a : b; }
short   max(short a, short b)   { return a >= b ? a : b; }

int main() {
    {
        int x = 10, y = 42;
        cout << "max("<<x<<", "<<y<<") = "<< max(x,y) << endl;
    }
    {
        long x = 512L, y = 123L;
        cout << "max("<<x<<", "<<y<<") = "<< max(x,y) << endl;
    }
    {
        float x = 2.71F, y = 3.14F;
        cout << "max("<<x<<", "<<y<<") = "<< max(x,y) << endl;
    }
    {
        double x = 2.71, y = 3.14;
        cout << "max("<<x<<", "<<y<<") = "<< max(x,y) << endl;
    }
    {
        string x = "abc"s, y = "ABC"s;
        cout << "max("<<x<<", "<<y<<") = "<< max(x,y) << endl;
    }
}


