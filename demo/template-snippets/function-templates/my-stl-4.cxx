#include <iostream>
#include "my-stl.hxx"

using namespace std;

int main() {
    auto const N = 20U;
    int        numbers[N]{};
    
    generate(numbers, numbers + N, [next = 0]() mutable { return ++next, next; });
    foreach(numbers, numbers + N, [](auto n) { cout << n << " "; });
    cout << endl;

    auto mapper  = [](auto n) { return n * n; };
    auto reducer = [](auto acc, auto n) { return acc + n; };
    auto result  = map_reduce<long long>(numbers, numbers + N, mapper, reducer);
    cout << "result: " << result << endl;
}

