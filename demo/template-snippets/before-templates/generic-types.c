extern int printf(const char*, ...);

#define mk_stack_type(Type, SIZE) \
    typedef struct {              \
        Type storage[SIZE];       \
        int  top;                 \
    } Stack_ ## Type;             \
    void  stack_ ## Type ## _init(Stack_ ## Type* this) { \
        this->top = 0;                                    \
    }                                                     \
    void  stack_ ## Type ## _push(Stack_ ## Type* this, Type x) { \
        this->storage[(this->top)++] = x;                         \
    }                                                             \
    Type  stack_ ## Type ## _pop(Stack_ ## Type* this) {  \
        return this->storage[--(this->top)];              \
    }                                                     \
    int  stack_ ## Type ## _empty(Stack_ ## Type* this) { \
        return this->top == 0;                            \
    }                                                     \
    int  stack_ ## Type ## _full(Stack_ ## Type* this) { \
        return this->top == (SIZE);                       \
    }                                                     \


mk_stack_type(int, 10)
mk_stack_type(double, 10)

int main() {
    {
        Stack_int stk;
        stack_int_init(&stk);
        for (int k=1; !stack_int_full(&stk); ++k)
            stack_int_push(&stk, k);
        while (!stack_int_empty(&stk))
            printf("%d ", stack_int_pop(&stk));
        printf("\n");
    }
    {
        Stack_double stk;
        stack_double_init(&stk);
        for (int k=1; !stack_double_full(&stk); ++k)
            stack_double_push(&stk, k * 3.1415);
        while (!stack_double_empty(&stk))
            printf("%.3f ", stack_double_pop(&stk));
        printf("\n");
    }
}

