extern int printf(const char*, ...);

#define mk_max_fn(Type) \
    Type max_ ## Type(Type a, Type b) {   \
        return ((a) >= (b)) ? (a) : (b);  \
    }

mk_max_fn(int)
mk_max_fn(long)
mk_max_fn(double)

int main() {
    {
        int a = 10, b = 12;
        printf("min(%d, %d) = %d\n", a, b, max_int(a,b));
    }
    {
        long a = 512, b = 63;
        printf("min(%ld, %ld) = %ld\n", a, b, max_long(a,b));
    }
    {
        double a = 2.71, b = 3.1415;
        printf("min(%.2f, %.2f) = %.2f\n", a, b, max_double(a,b));
    }
}

