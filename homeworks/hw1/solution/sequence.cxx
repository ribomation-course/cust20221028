#include <iostream>
#include <string>
using std::cout;
using std::string;
using namespace std::string_literals;

template<typename ElemType, typename NextFn>
class Sequence {
    ElemType start;
    NextFn   nextFn;
    unsigned maxCount;
public:
    Sequence(ElemType start_, NextFn nextFn_, unsigned maxCount_ = 10)
            : start{start_}, nextFn{nextFn_}, maxCount{maxCount_} {}

    struct iterator {
        Sequence* seq;
        unsigned count;
        ElemType current;

        iterator(Sequence* const seq_) : seq(seq_), count{0}, current{seq->start} {}
        iterator(unsigned count_)      : count{count_} {}

        bool     operator!=(iterator rhs) const { return count != rhs.count; }
        ElemType operator*() const { return current; }
        void     operator++() {
            current = seq->nextFn(current);
            ++count;
        }
    };

    iterator begin() { return {this}; }
    iterator end()   { return {maxCount}; }
};


int main() {
    {
        auto      next = [](auto n) { return n + 2; };
        for (auto n: Sequence{2, next}) cout << n << ", ";
        cout << "\n";
    }
    {
        auto      next = [](auto n) { return 2 * n + 1; };
        for (auto n: Sequence{1, next}) cout << n << ", ";
        cout << "\n";
    }
    {
        auto      next = [f0 = 0](auto f1) mutable {
            auto f = f0 + f1;
            f0 = f1;
            return f;
        };
        for (auto n: Sequence{1, next}) cout << n << ", ";
        cout << "\n";
    }
    {
        auto next = [](auto s){ auto ch = s.back(); ++ch; return s + ch;};
        for (auto n: Sequence{"A"s, next}) cout << n << ", ";
        cout << "\n";
    }
}
