#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>

using namespace std::string_literals;
using std::cout;
using std::string;

template<typename Key, typename Value>
auto operator<<(std::ostream& os, std::pair<Key, Value> const& pair) -> std::ostream& {
    return os << pair.first << ":" << pair.second;
}

template<typename Container>
void print(string const& name, Container cont) {
    cout << name << ": ";
    bool delim = false;
    for (auto const& elem: cont) {
        if (delim) cout << ", "; else delim = true;
        cout << elem;
    }
    cout << "\n";
}


int main() {
    {
        auto vec = std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        print("vec  ", vec);
    }
    {
        auto deque = std::deque<int>{11, 22, 33, 44, 55, 66, 77, 88, 99, 1010};
        print("deque", deque);
    }
    {
        auto list = std::list<string>{"Anna"s, "Berit"s, "Carin"s, "Doris"s};
        print("list ", list);
    }
    {
        auto fwlst = std::forward_list<string>{"Adam"s, "Bertil"s, "Carl"s, "Dan"s};
        print("fwlst", fwlst);
    }
    {
        auto set = std::set<string>{"Hamlet"s, "Aramis"s, "Romeo"s, "Juliet"s};
        print("set  ", set);
    }
    {
        auto unset = std::unordered_set<string>{"Hamlet"s, "Aramis"s, "Romeo"s, "Juliet"s};
        print("unset", unset);
    }
    {
        auto str = string{"Also a container"};
        print("str  ", str);
    }
    {
        auto map = std::map<string, unsigned>{
                {"Hamlet"s,      113},
                {"Romeo"s,       97},
                {"Richard III"s, 83},
                {"Juliet"s,      101},
                {"Aramis"s,      67}
        };
        print("map  ", map);
    }
    {
        auto unmap = std::unordered_map<string, unsigned>{
                {"Hamlet"s,      113},
                {"Romeo"s,       97},
                {"Richard III"s, 83},
                {"Juliet"s,      101},
                {"Aramis"s,      67}
        };
        print("unmap", unmap);
    }
}
