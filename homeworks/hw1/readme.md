# Homework 1

## Task description 1

Design and implement a generic `print` function that can print out
the content of an STL container object.

## Sample Use Cases

    auto vec   = std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto deque = std::deque<int>{11, 22, 33, 44, 55, 66, 77, 88, 99, 1010};
    auto list  = std::list<string>{"Anna"s, "Berit"s, "Carin"s, "Doris"s};
    auto fwlst = std::forward_list<string>{"Adam"s, "Bertil"s, "Carl"s, "Dan"s};
    auto set   = std::set<string>{"Hamlet"s, "Aramis"s, "Romeo"s, "Juliet"s};
    auto unset = std::unordered_set<string>{"Hamlet"s, "Aramis"s, "Romeo"s, "Juliet"s};
    auto str   = std::string{"Also a container"};
    auto map   = std::map<string, unsigned>{
        {"Hamlet"s, 113}, {"Romeo"s, 97}, {"Richard III"s, 83}, {"Juliet"s, 101}, {"Aramis"s, 67}
    };
    auto unmap = std::unordered_map<string, unsigned>{
        {"Hamlet"s, 113}, {"Romeo"s, 97}, {"Richard III"s, 83}, {"Juliet"s, 101}, {"Aramis"s, 67}
    };


## Task description 2

Design and implement a generic implicit sequence data-type that supports 
the `for-each` loop

    auto data = MyDataType{...args...};
    for (auto n : data) cout << n << ", ";

An _implicit_ data-type means that it doesn't store all values, just
how to compute a value when asked for it.

A _sequence_ data-type has a start value, and a next function plus
optionally a max count. 

_For example: Even Numbers_
* `start = 2`
* `next  = (n) -> n+2`
* `max   = 5`
* **Result:** 2 4 6 8 10

### Understanding the `for-each` loop
When you write a for-each loop like

    auto vec = std::vector<int>{1,2,3,4,5};
    for (auto n : vec) std::cout << n << ", ";

It gets translated into the following code

    auto vec = std::vector<int>{1,2,3,4,5};
    for (auto it = vec.begin(); it != vec.end(); ++it) {
        auto n = *it;
        std::cout << n << ", ";
    }

Which in the next phase of translation becomes

    std::vector<int> vec{1,2,3,4,5};
    for (std::vector<int>::iterator it = vec.begin(); it.operator !=(vec.end()); it.operator++()) {
        int n = it.operator*();
        operator <<((operator <<(std::cout, n), ", ");
    }

That means, your data-type should have two methods `begin()` and `end()`,
that returns an iterator object. The iterator class is defined as public
within the class that provides it.

The iterator class should support the following operators:

* `bool operator !=(iterator rhs)` : Returns true as long the iteration should proceed
* `Type operator *()` : Returns the current value/object
* `void operator ++()` : Advances to the next value/object

### Sample Use Cases

    auto nxt1 = [](auto n) { return n + 2; };
    auto nxt2 = [](auto n) { return 2 * n + 1; };
    auto nxt3 = [f0 = 0](auto f1) mutable { auto f = f0 + f1; f0 = f1; return f; };
    auto next = [](auto s){ auto ch = s.back(); ++ch; return s + ch;};

