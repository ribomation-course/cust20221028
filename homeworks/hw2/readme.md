# Homework 2

## Task description 

Design and implement fixed capacity vector container, with support for
emplacement and a bit-vector specialization for element type `bool`.

## Use Cases 1

    template<typename ElemType, auto CAPACITY>
    class Vector {
        ElemType    storage[CAPACITY]{};
        //...
    };

    auto vec = Vector<int, 10>{};
    for (auto k=10; !vec.full(); k += 10) vec.put(k);
    while (!vec.empty()) std::cout << vec.get() << "\n";

    auto people = Vector<Person, 3>{};
    people.emplace("Anna"s, 42, false);
    people.emplace("Bertil"s, 52, true);
    people.put( Person{"Carin"s, 52, false} );
    for (auto const& p : people) std::cout << p << "\n";

## Use Cases 2
When the element type is `bool`, it should use store 8 booleans per
byte, i.e., you need to implement a bit-vector functionality. This is,
by the way, how [`std::vector<bool>`](https://en.cppreference.com/w/cpp/container/vector_bool) 
is implemented. You might also want to take a look at 
[`std::bitset<N>`](https://en.cppreference.com/w/cpp/utility/bitset).

    auto bits = Vector<bool, 64>{};
    bits = 0b10101010101010;
    for (auto b : bits) std::cout << (b ? "#" : ".");

There is no need to implement method `emplace()` for this specialization.

## Advice
Feel free to both add methods and modify/remove methods as you see fit.
The purpose here is _emplacement_ and _specialization_, so you do not need
to make the data-type feature complete.


