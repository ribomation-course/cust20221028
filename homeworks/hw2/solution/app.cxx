#include <iostream>
#include <string>
#include <stdexcept>
#include "vector.hxx"
#include "person.hxx"

using std::cout;
using std::string;
using ribomation::utils::Vector;
using ribomation::domain::Person;
using namespace std::string_literals;


void usecase_int() {
    cout << "-- int --\n";
    auto      vec = Vector<int, 10U>{};
    for (auto k   = 10; !vec.full(); k += 10) vec.put(k);
    while (!vec.empty()) cout << vec.get() << " ";
    cout << "\n";
}

void usecase_over_under_flow() {
    cout << "-- over/under flow --\n";
    auto vec = Vector<int, 1U>{};
    try {
        auto x = vec.get();
        cout << "should not happen: " << x;
    } catch (std::underflow_error const& x) {
        cout << "OK, got underflow\n";
    }
    try {
        vec.put(1);
        vec.put(2);
        cout << "should not happen: ";
    } catch (std::overflow_error const& x) {
        cout << "OK, got overflow\n";
    }
}

void usecase_person() {
    cout << "-- person --\n";
    auto vec = Vector<Person, 4U>{};
    vec.emplace("Anna"s, 42, true);
    vec.emplace("Bertil"s, 44, false);
    vec.emplace("Carin"s, 46, true);
    vec.emplace("Daniel"s, 48, false);
    while (!vec.empty()) cout << vec.get() << " ";
    cout << "\n";
}

void usecase_bitvec() {
    cout << "-- bitvec --\n";
    auto bits = Vector<bool, 64U>{};
    bits = 0b10101010101010;
    cout << bits << "\n";
    for (auto b: bits) cout << (b ? "#" : ".");
}

int main() {
    usecase_int();
    usecase_person();
    usecase_over_under_flow();
    usecase_bitvec();
}
