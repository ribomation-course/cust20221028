#pragma once
#include <iostream>
#include <string>

namespace ribomation::domain {
    using std::string;
    
    struct Person {
        string name;
        int    age;
        bool   female;
        Person() = default;

        Person(const string& name, int age, bool female)
                : name(name), age(age), female(female) {}
    };

    auto operator<<(std::ostream& os, Person const& p) -> std::ostream& {
        return os << "Person{" << p.name << ", " << p.age << ", " << (p.female ? "Female" : "Male") << "}";
    }

}

