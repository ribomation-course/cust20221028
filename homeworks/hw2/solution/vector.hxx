#pragma once
#include <iostream>
#include <bitset>

namespace ribomation::utils {
    using namespace std::string_literals;
    
    template<typename ElemType, auto CAPACITY>
    class Vector {
        ElemType storage[CAPACITY]{};
        int      putIdx = 0;
        int      getIdx = 0;
        int      count  = 0;
    public:
        Vector() = default;
        int size() const { return count; }
        int capacity() const { return CAPACITY; }
        bool empty() const { return size() == 0; }
        bool full() const { return size() == capacity(); }

        void put(ElemType x) {
            if (full()) throw std::overflow_error{"vector full"s};
            storage[putIdx++] = x;
            putIdx %= CAPACITY;
            ++count;
        }

        template<typename... ConstructorTypes>
        void emplace(ConstructorTypes... args) {
            if (full()) throw std::overflow_error{"vector full"s};
            auto addr = &storage[putIdx++];
            new(addr) ElemType{args...};
            putIdx %= CAPACITY;
            ++count;
        }

        ElemType get() {
            if (empty()) throw std::underflow_error{"vector empty"s};
            auto x = storage[getIdx++];
            getIdx %= CAPACITY;
            --count;
            return x;
        }
    };


    template<auto CAPACITY>
    class Vector<bool, CAPACITY> {
        std::bitset<CAPACITY> storage{};
        int                   putIdx = 0;
        int                   getIdx = 0;
        int                   count  = 0;
    public:
        Vector() = default;

        void operator=(unsigned long long value) {
            storage = value;
            while ((value >>= 1) != 0) {
                ++count;
                ++putIdx;
            }
        }

        void put(bool x) {
            storage[putIdx++] = x ? 1 : 0;
            putIdx %= CAPACITY;
            ++count;
        }

        bool get() {
            bool x = storage[getIdx++] == 1;
            getIdx %= CAPACITY;
            --count;
            return x;
        }

        struct iterator {
            Vector<bool, CAPACITY> const* vec;
            int current = 0;

            iterator(Vector<bool, CAPACITY> const* vec_) : vec{vec_} {}
            iterator(int max) : current{max} {}

            bool operator!=(iterator const& rhs) const { return current != rhs.current; }
            bool operator*() const { return vec->storage[current]; }
            void operator ++() { ++current; }
        };

        iterator begin() const { return {this}; }
        iterator end() const { return {count}; }

        friend auto operator <<(std::ostream& os, Vector<bool, CAPACITY> const& v) -> std::ostream& {
            return os << "[" << v.storage << "]";
        }
    };

}

