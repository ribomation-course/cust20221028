![C++ Templates](img/cxx-templates-logo.png)
# C++ Templates, 2022 November

# Links
* [Course Details](https://www.ribomation.se/programmerings-kurser/cxx/cxx-templates/)
* [Installation Instructions](./installation-instructions.md)
* [Homework 1](./homeworks/hw1/readme.md) - [Solution](./homeworks/hw1/solution)
* [Homework 2](./homeworks/hw2/readme.md) - [Solution](./homeworks/hw2/solution)


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated.
Create a dedicated directory for this course and a sub-directory for
each chapter. Get the course repo initially by a `git clone` operation

![Git Clone](img/git-clone.png)

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/cxx-course/gitlab
    git pull


# Build Solution/Demo Programs

The solutions and demo programs are all using CMake as the build tool. CMake is
a cross-platform generator
tool that can generate makefiles and other build tool files. It is also the
project descriptor for JetBrains
CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources.
What you do need to have; are `cmake`, `make` and `gcc/g++` all installed.
When you want to build a solution or demo:

First change into its project directory `cd path/to/some/solutions/dir`, then
run the commands below
and the executable will be in the `./bld/` directory.

    cd to/some/project/dir/
    cmake -S . -B bld
    cmake --build bld


# Interesting Videos & Articles

## Deep Dive
* [Building GCC, yourself](https://solarianprogrammer.com/2016/10/07/building-gcc-ubuntu-linux/)

## Online Compilers and Tools
* [Compiler Explorer](https://godbolt.org/)
* [C++ Insights](https://cppinsights.io/)
* [C++ Quick Benchmarks](http://quick-bench.com/)
* [Coliru - Online Compiler](https://coliru.stacked-crooked.com/)
* [WandBox - Online Compiler](https://wandbox.org/)

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

